# M2C Modula-2 Compiler & Translator (m2c)
#
# This file was contributed by Peter Eiserloh and modified by B.Kowarsch
# Copyright (c) 2009, 2010, 2015 P.Eiserloh & B.Kowarsch
#
# @synopsis
#
# M2C is a compiler and translator for the classic Modula-2 programming
# language as described in the 3rd and 4th editions of Niklaus Wirth's
# book "Programming in Modula-2" (PIM) published by Springer Verlag.
#
# In compiler mode, M2C compiles Modula-2 source via C to object files or
# executables using the host system's resident C compiler and linker.
# In translator mode, it translates Modula-2 source to C source.
#
# Further information at http://savannah.nongnu.org/projects/m2c/
#
# @file
#
# Makefile
#
# Makefile for use with the GCC toolchain
#
# License:
#
# M2C is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation;  either version 2 of the License (GPL2),
# or (at your option) any later version.
# 
# M2C is distributed in the hope that it will be useful,  but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License along
# with m2c.  If not, see <http://www.gnu.org/licenses/>.


# ---------------------------------------------------------------------------
# A P P L I C A T I O N S
# ---------------------------------------------------------------------------

APP_1 = m2c

APP_2 = testlex

APP_3 = gen_first_sets

APP_4 = gen_follow_sets

APP_5 = gen_resync_sets

APP_6 = print_first_sets

APP_7 = print_follow_sets

SRC_1 = \
	m2-lexer.c \
	m2-filereader.c \
	m2-fileutils.c \
	m2-token.c \
	m2-tokenset.c \
	m2-error.c \
	m2-production.c \
	m2-resync-sets.c \
	m2-parser.c \
	m2-unique-string.c \
	m2-compiler-options.c \
	m2c.c

SRC_2 = \
	m2-lexer.c \
	m2-filereader.c \
	m2-fileutils.c \
	m2-token.c \
	m2-tokenset.c \
	m2-error.c \
	m2-unique-string.c \
	m2-compiler-options.c \
	testlex.c

SRC_3 = \
	m2-token.c \
	m2-tokenset.c \
	gen_first_sets.c

SRC_4 = \
	m2-token.c \
	m2-tokenset.c \
	gen_follow_sets.c

SRC_5 = \
	m2-token.c \
	m2-tokenset.c \
	gen_resync_sets.c

SRC_6 = \
	m2-token.c \
	m2-tokenset.c \
	m2-production.c \
	m2-error.c \
	m2-compiler-options.c \
	print_first_sets.c

SRC_7 = \
	m2-token.c \
	m2-tokenset.c \
	m2-production.c \
	m2-error.c \
	m2-compiler-options.c \
	print_follow_sets.c

LIB =


# ---------------------------------------------------------------------------
# T O O L S
# ---------------------------------------------------------------------------

RM = rm -f

CC = gcc
CFLAGS = -std=c99 -Wall -Wno-comments -g

LD = gcc
LDFLAGS =


# ---------------------------------------------------------------------------
# A U T O M A T I C -- D O  N O T  E D I T  B E L O W  T H I S  L I N E
# ---------------------------------------------------------------------------


# ---------------------------------------------------------------------------
# S O U R C E  C L A S S I F I C A T I O N
# ---------------------------------------------------------------------------

C_SOURCE_1 = $(filter %.c,$(SRC_1))
C_SOURCE_2 = $(filter %.c,$(SRC_2))
C_SOURCE_3 = $(filter %.c,$(SRC_3))
C_SOURCE_4 = $(filter %.c,$(SRC_4))
C_SOURCE_5 = $(filter %.c,$(SRC_5))
C_SOURCE_6 = $(filter %.c,$(SRC_6))
C_SOURCE_7 = $(filter %.c,$(SRC_7))
C_SOURCE = $(C_SOURCE_1) $(C_SOURCE_2) $(C_SOURCE_3) $(C_SOURCE_4) $(C_SOURCE_5) $(C_SOURCE_6) $(C_SOURCE_7)
C_HEADER = *.h


# ---------------------------------------------------------------------------
# O B J E C T  C L A S S I F I C A T I O N
# ---------------------------------------------------------------------------

C_OBJECT_1 = $(C_SOURCE_1:%.c=%.o)
C_OBJECT_2 = $(C_SOURCE_2:%.c=%.o)
C_OBJECT_3 = $(C_SOURCE_3:%.c=%.o)
C_OBJECT_4 = $(C_SOURCE_4:%.c=%.o)
C_OBJECT_5 = $(C_SOURCE_5:%.c=%.o)
C_OBJECT_6 = $(C_SOURCE_6:%.c=%.o)
C_OBJECT_7 = $(C_SOURCE_7:%.c=%.o)


# ---------------------------------------------------------------------------
# M A I N  T A R G E T S
# ---------------------------------------------------------------------------

.PHONY: all clean tidy summary

all: $(APP_1) $(APP_2) $(APP_3) $(APP_4) $(APP_5) $(APP_6) $(APP_7)

clean:
	$(RM) $(C_OBJECT_1) $(C_OBJECT_2) $(C_OBJECT_3) $(C_OBJECT_4) $(C_OBJECT_5) $(C_OBJECT_6) $(C_OBJECT_7)

tidy: clean
	$(RM) depends.mk *.o $(APP_1) $(APP_2) $(APP_3) $(APP_4) $(APP_5) $(APP_6) $(APP_7)

summary:
	@ echo C Headers = $(C_HEADER)
	@ echo C Sources = $(C_SOURCE_1) $(C_SOURCE_2) $(C_SOURCE_3) $(C_SOURCE_4) $(C_SOURCE_5) $(C_SOURCE_6) $(C_SOURCE_7)
	@ echo C Objects = $(C_OBJECT_1) $(C_OBJECT_2) $(C_OBJECT_3) $(C_OBJECT_4) $(C_OBJECT_5) $(C_OBJECT_6) $(C_OBJECT_7)
	@ echo Libraries = $(LIB)


# ---------------------------------------------------------------------------
# B U I L D  T A R G E T S
# ---------------------------------------------------------------------------

%.o : %.c
	$(CC) -c $(CFLAGS) $< -o $@

$(APP_1) : $(C_OBJECT_1)
	$(LD) $(LDFLAGS) $(LIB:%=-l%) $^ -o $@

$(APP_2) : $(C_OBJECT_2)
	$(LD) $(LDFLAGS) $(LIB:%=-l%) $^ -o $@

$(APP_3) : $(C_OBJECT_3)
	$(LD) $(LDFLAGS) $(LIB:%=-l%) $^ -o $@

$(APP_4) : $(C_OBJECT_4)
	$(LD) $(LDFLAGS) $(LIB:%=-l%) $^ -o $@

$(APP_5) : $(C_OBJECT_5)
	$(LD) $(LDFLAGS) $(LIB:%=-l%) $^ -o $@

$(APP_6) : $(C_OBJECT_6)
	$(LD) $(LDFLAGS) $(LIB:%=-l%) $^ -o $@

$(APP_7) : $(C_OBJECT_7)
	$(LD) $(LDFLAGS) $(LIB:%=-l%) $^ -o $@


# ---------------------------------------------------------------------------
# D E P E N D E N C I E S
# ---------------------------------------------------------------------------

depends.mk : Makefile $(C_SOURCE) $(C_HEADER)
	$(CC) -MM $(C_SOURCE) > depends.mk

-include depends.mk


# END OF FILE