/* M2C Modula-2 Compiler & Translator
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * M2C is a compiler and translator for the classic Modula-2 programming
 * language as described in the 3rd and 4th editions of Niklaus Wirth's
 * book "Programming in Modula-2" (PIM) published by Springer Verlag.
 *
 * In compiler mode, M2C compiles Modula-2 source via C to object files or
 * executables using the host system's resident C compiler and linker.
 * In translator mode, it translates Modula-2 source to C source.
 *
 * Further information at http://savannah.nongnu.org/projects/m2c/
 *
 * @file
 *
 * m2-fileutils.h
 *
 * File system utility functions.
 *
 * @license
 *
 * M2C is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation;  either version 2 of the License (GPL2),
 * or (at your option) any later version.
 *
 * M2C is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with m2c.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef M2C_FILEUTILS_H
#define M2C_FILEUTILS_H

#include <stddef.h>
#include <stdbool.h>


/* --------------------------------------------------------------------------
 * Invalid file size indicator
 * ----------------------------------------------------------------------- */

#define INVALID_FILE_SIZE -1


/* --------------------------------------------------------------------------
 * function is_valid_filename(filename)
 * --------------------------------------------------------------------------
 * Returns true if filename is a valid filename, otherwise false.
 * ----------------------------------------------------------------------- */

bool is_valid_filename (const char *filename);


/* --------------------------------------------------------------------------
 * function is_valid_pathname(pathname)
 * --------------------------------------------------------------------------
 * Returns true if pathname is a valid pathname, otherwise false.
 * ----------------------------------------------------------------------- */

bool is_valid_pathname (const char *pathname);


/* --------------------------------------------------------------------------
 * function file_exists(filename)
 * --------------------------------------------------------------------------
 * Returns true if the file indicated by filename exists, otherwise false.
 * ----------------------------------------------------------------------- */

bool file_exists (const char *filename);


/* --------------------------------------------------------------------------
 * function filesize(filename)
 * --------------------------------------------------------------------------
 * Returns the file size of a file.  Returns INVALID_FILE_SIZE on failure.
 * ----------------------------------------------------------------------- */

int filesize (const char *filename);


/* --------------------------------------------------------------------------
 * function filetype(filename)
 * --------------------------------------------------------------------------
 * Returns a pointer to the position of the last period in filename.
 * Returns NULL if filename is NULL or if it does not contain any period.
 * ----------------------------------------------------------------------- */

const char *filetype (const char *filename);


/* --------------------------------------------------------------------------
 * function macro get_cwd(cwd, size)
 * --------------------------------------------------------------------------
 * Obtains current directory path in cwd and returns a pointer to it.
 * ----------------------------------------------------------------------- */

char *get_cwd(char *cwd, size_t size);

#endif /* M2C_FILEUTILS_H */

/* END OF FILE */