This repo hosts replacement files for the m2c Modula-2 compiler originally written by V.Makarov, recently forked by D.Evans.

[http://savannah.nongnu.org/projects/m2c](http://savannah.nongnu.org/projects/m2c)

This effort was an experiment to gauge how much work it might be to overhaul Makarov's compiler and the conclusion was
that it would be more work than writing a new compiler from scratch, thus for now this is put in quasi-abandonment.
