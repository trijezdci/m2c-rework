/* M2C Modula-2 Compiler & Translator
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * M2C is a compiler and translator for the classic Modula-2 programming
 * language as described in the 3rd and 4th editions of Niklaus Wirth's
 * book "Programming in Modula-2" (PIM) published by Springer Verlag.
 *
 * In compiler mode, M2C compiles Modula-2 source via C to object files or
 * executables using the host system's resident C compiler and linker.
 * In translator mode, it translates Modula-2 source to C source.
 *
 * Further information at http://savannah.nongnu.org/projects/m2c/
 *
 * @file
 *
 * m2-parser.h
 *
 * Public interface for M2C parser module.
 *
 * @license
 *
 * M2C is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation;  either version 2 of the License (GPL2),
 * or (at your option) any later version.
 *
 * M2C is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with m2c.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef M2C_PARSER_H
#define M2C_PARSER_H

#include "m2-common.h"
#include "m2-unique-string.h"


/* temporary dummy type */
typedef int *m2c_ast_t; /* TO DO: AST module */


/* --------------------------------------------------------------------------
 * type m2c_parser_status_t
 * --------------------------------------------------------------------------
 * Status codes for operations on type m2c_parser_t.
 * --------------------------------------------------------------------------
 */

typedef enum {
  M2C_PARSER_STATUS_SUCCESS,
  M2C_PARSER_STATUS_INVALID_REFERENCE,
  M2C_PARSER_STATUS_ALLOCATION_FAILED,
  /* TO DO : add status codes for all error scenarios */
} m2c_parser_status_t;


/* --------------------------------------------------------------------------
 * procedure m2c_syntax_check_def(filename, status, err_count)
 * --------------------------------------------------------------------------
 * Syntax checks a Modula-2 .DEF file represented by filename.
 * ----------------------------------------------------------------------- */

void m2c_syntax_check_def
  (m2c_string_t filename, m2c_parser_status_t *status, uint_t *err_count);


/* --------------------------------------------------------------------------
 * procedure m2c_syntax_check_mod(filename, status, err_count)
 * --------------------------------------------------------------------------
 * Syntax checks a Modula-2 .MOD file represented by filename.
 * ----------------------------------------------------------------------- */

void m2c_syntax_check_mod
  (m2c_string_t filename, m2c_parser_status_t *status, uint_t *err_count);


/* --------------------------------------------------------------------------
 * function m2c_parse_def(filename, status)
 * --------------------------------------------------------------------------
 * Parses a Modula-2 .DEF file represented by filename and returns an AST.
 * ----------------------------------------------------------------------- */

m2c_ast_t m2c_parse_def
  (m2c_string_t filename, m2c_parser_status_t *status);


/* --------------------------------------------------------------------------
 * function m2c_parse_def(filename, status)
 * --------------------------------------------------------------------------
 * Parses a Modula-2 .MOD file represented by filename and returns an AST.
 * ----------------------------------------------------------------------- */

m2c_ast_t m2c_parse_mod
  (m2c_string_t filename, m2c_parser_status_t *status);


#endif /* M2C_PARSER_H */

/* END OF FILE */