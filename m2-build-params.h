/* m2-build-params.h -- build parameters for m2c -- public domain file */

#ifndef M2C_BUILD_PARAMS_H
#define M2C_BUILD_PARAMS_H

#include "m2-build-param-values.h"

#define M2C_BUILD_NUMBER 1

#define M2C_TARGET_OS OS_TYPE_POSIX
#define M2C_TARGET_FILENAMING FILENAMING_POSIX

#define M2C_PATHNAME_BUFFER_SIZE 1024

#define M2C_COROUTINES_IMPLEMENTED 0
#define M2C_LOCAL_MODULES_IMPLEMENTED 0

#endif /* M2C_BUILD_PARAMS_H */

/* END OF FILE */