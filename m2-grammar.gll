/* M2C Modula-2 Compiler & Translator
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * M2C is a compiler and translator for the classic Modula-2 programming
 * language as described in the 3rd and 4th editions of Niklaus Wirth's
 * book "Programming in Modula-2" (PIM) published by Springer Verlag.
 *
 * In compiler mode, M2C compiles Modula-2 source via C to object files or
 * executables using the host system's resident C compiler and linker.
 * In translator mode, it translates Modula-2 source to C source.
 *
 * Further information at http://savannah.nongnu.org/projects/m2c/
 *
 * @file
 *
 * grammar.gll
 *
 * Grammar of Modula-2 source files.
 *
 * @license
 *
 * M2C is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation;  either version 2 of the License (GPL2),
 * or (at your option) any later version.
 *
 * M2C is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with m2c.  If not, see <http://www.gnu.org/licenses/>.
 */

grammar Modula2;

/*** Reserved Words ***/

reserved
  AND, ARRAY, BEGIN, BY, CASE, CONST, DEFINITION, DIV, DO, ELSE, ELSIF, END,
  EXIT, EXPORT, FOR, FROM, IF, IMPLEMENTATION, IMPORT, IN, LOOP, MOD,
  MODULE, NOT, OF, OR, POINTER, PROCEDURE, QUALIFIED, RECORD, REPEAT,
  RETURN, SET, THEN, TO, TYPE, UNTIL, VAR, WHILE, WITH;


/*** Non-Terminal Symbols ***/

/* Compilation Unit */

compilationUnit :=
  definitionModule | implementationModule | programModule
  ;


/* Definition Module */

definitionModule :=
  DEFINITION MODULE moduleIdent ';'
  import* definition* END moduleIdent '.'
  ;

alias moduleIdent = Ident ;


/* Import */

import :=
  ( qualifiedImport | unqualifiedImport ) ';'
  ;


/* Qualified Import */

qualifiedImport :=
  IMPORT moduleList
  ;

alias moduleList = identList;


/* Unqualified Import */

unqualifiedImport :=
  FROM moduleIdent IMPORT identList
  ;


/* Identifier List */

identList :=
  Ident ( ',' Ident )*
  ;


/* Definition */

definition :=
  CONST ( constDefinition ';' )* |
  TYPE ( typeDefinition ';' )* |
  VAR ( varDefinition ';' )* |
  procedureHeader ';'
  ;

alias varDefinition = variableDeclaration ;


/* Constant Definition */

constDefinition :=
  Ident '=' constExpression
  ;


/* Type Definition */

typeDefinition :=
  Ident ( '=' type )?
  ;


/* Type */

type :=
  derivedOrSubrangeType | enumType | setType | arrayType |
  recordType | pointerType | procedureType
  ;


/* Derived Type or Subrange Type */

derivedOrSubrangeType :=
  typeIdent range? | range
  ;

alias typeIdent = qualident ;


/* Qualified Identifier */

qualident :=
  Ident ( '.' Ident )*
  ;


/* Range */

range :=
  '[' constExpression '..' constExpression ']'
  ;


/* Enumeration Type */

enumType :=
  '(' identList ')'
  ;


/* Set Type */

setType :=
  SET OF countableType
  ;


/* Countable Type */

countableType :=
  range | enumType | typeIdent range?
  ;


/* Array Type */

arrayType :=
  ARRAY OF countableType ( ',' countableType )* OF type
  ;


/* Extensible Record Type */

extensibleRecordType :=
  RECORD ( '(' baseType ')' )? fieldListSequence END
  ;

alias baseType = typeIdent ;


/* Field List Sequence */

fieldListSequence :=
  fieldList ( ';' fieldList )*
  ;

alias fieldList = variableDeclaration ;


/* Variant Record Type */

variantRecordType :=
  RECORD variantFieldListSeq END
  ;


/* Variant Record Field List Sequence */

variantFieldListSeq :=
  variantFieldList ( ';' variantFieldList )*
  ;


/* Variant Record Field List */

variantFieldList :=
  fieldList | variantFields
  ;


/* Variant Fields */

variantFields :=
  CASE Ident? ':' typeIdent OF
  variant ( '|' variant )*
  ( ELSE fieldListSequence )?
  END
  ;


/* Variant */

variant :=
  caseLabelList ':' variantFieldListSeq
  ;


/* Case Label List */

caseLabelList :=
  caseLabels ( ',' caseLabels )*
  ;


/* Case Labels */

caseLabels :=
  constExpression ( '..' constExpression )?
  ;


/* Pointer Type */

pointerType :=
  POINTER TO type
  ;


/* Procedure Type */

procedureType :=
  PROCEDURE ( '(' ( formalType ( ',' formalType )* )? ')' )?
  ( ':' returnedType )?
  ;


/* Formal Type */

formalType :=
  simpleFormalType | attributedFormalType
  ;


/* Simple Formal Type */

simpleFormalType :=
  ( ARRAY OF )? typeIdent
  ;


/* Attributed Formal Type */

attributedFormalType :=
  ( CONST | VAR ) simpleFormalType
  ;


/* Procedure Header */

procedureHeader :=
  PROCEDURE procedureSignature
  ;


/* Procedure Signature */

procedureSignature :=
  Ident ( '(' formalParamList? ')' ( ':' returnedType )? )?
  ;


/* Formal Parameter List */

formalParamList :=
  formalParams ( ';' formalParams )*
  ;


/* Formal Parameters */

formalParams :=
  simpleFormalParams | attribFormalParams
  ;


/* Simple Formal Parameters */

simpleFormalParams :=
  identList ':' formalType
  ;


/* Attributed Formal Parameters */

attribFormalParams :=
  ( CONST | VAR ) simpleFormalParams
  ;


/* Implementation Module */

implementationModule :=
  IMPLEMENTATION programModule
  ;


/* Program Module */

programModule :=
  MODULE moduleIdent modulePriority? ';'
  import* block moduleIdent '.'
  ;


/* Module Priority */

modulePriority :=
  '[' constExpression ']'
  ;


/* Block */

block :=
  declaration* ( BEGIN statementSequence )? END
  ;


/* Declaration */

declaration :=
  CONST ( constDeclaration ';' )* |
  TYPE ( typeDeclaration ';' )* |
  VAR ( variableDeclaration ';' )* |
  procedureDeclaration ';'
  moduleDeclaration ';'
  ;


/* Type Declaration */

typeDeclaration :=
  Ident '=' ( type | varSizeRecordType )
  ;

/* Variable Size Record Type */

varSizeRecordType :=
  VAR RECORD fieldListSequence
  VAR Ident ':' ARRAY sizeFieldIdent OF typeIdent
  END
  ;

alias sizeFieldIdent = Ident;


/* Variable Declaration */

variableDeclaration :=
  identList ':' type
  ;


/* Procedure Declaration */

procedureDeclaration :=
  procedureHeader ';' block Ident
  ;


/* Module Declaration */

moduleDeclaration :=
  MODULE moduleIdent modulePriority? ';'
  import* export? block moduleIdent
  ;


/* Export */

export :=
  EXPORT QUALIFIED? identList ';'
  ;


/* Statement Sequence */

statementSequence :=
  statement ( ';' statement )*
  ;


/* Statement */

statement :=
  assignmentOrProcCall | returnStatement | withStatement | ifStatement |
  caseStatement | loopStatement | whileStatement | repeatStatement |
  forStatement | EXIT
  ;


/* Assignment Or Procedure Call */

assignmentOrProcCall :=
  designator ( ':=' expression | actualParameters )?
  ;


/* Actual Parameters */

actualParameters :=
  '(' expressionList? ')'
  ;


/* Expression List */

expressionList :=
  expression ( ',' expression )*
  ;


/* RETURN Statement */

returnStatement :=
  RETURN expression?
  ;


/* WITH Statement */

withStatement :=
  WITH designator DO statementSequence END
  ;


/* IF Statement */

ifStatement :=
  IF boolExpression THEN statementSequence
  ( ELSIF boolExpression THEN statementSequence )*
  ( ELSE statementSequence )?
  END
  ;

alias boolExpression = expression ;


/* CASE Statement */

caseStatement :=
  CASE expression OF case ( '|' case )*
  ( ELSE statementSequence )?
  END
  ;


/* Case */

case :=
  caseLabelList ':' statementSequence
  ;


/* LOOP Statement */

loopStatement :=
  LOOP statementSequence END
  ;


/* WHILE Statement */

whileStatement :=
  WHILE boolExpression DO statementSequence END
  ;


/* REPEAT Statement */

repeatStatement :=
  REPEAT statementSequence UNTIL boolExpression
  ;


/* FOR Statement */

forStatement :=
  FOR forLoopVariant ':=' startValue TO endValue
  ( BY stepValue )? DO statementSequence END
  ;

alias forLoopVariant = Ident ;

alias startValue, endValue = ordinalExpression ;

alias ordinalExpression = expression ;

alias stepValue = constExpression ;


/* Designator */

designator :=
  qualident selector*
  ;


/* Selector */

selector :=
  '^' | '.' Ident | '[' expressionList ']'
  ;


/* Expression */

expression :=
  simpleExpression ( OperL1 simpleExpression )?
  ;

.OperL1 = := '=' | '#' | '<' | '<=' | '>' | '>=' | IN ;


/* Simple Expression */

simpleExpression :=
  ( '+' | '-' )? term ( OperL2 term )*
  ;

.OperL2 = := '+' | '-' | OR ;


/* Term */

term :=
  simpleTerm ( OperL3 simpleTerm )*
  ;

.OperL3 = := '*' | '/' | DIV | MOD | AND ;


/* Simple Term */

simpleTerm :=
  NOT? factor
  ;


/* Factor */

factor :=
  NumberLiteral | StringLiteral | setValue |
  designatorOrFuncCall | '(' expression ')'
  ;


/* Designator Or Function Call */

designatorOrFuncCall :=
  designator ( setValue | '(' expressionList? ')' )?
  ;


/* Set Value */

setValue :=
  '{' element ( ',' element )* '}'
  ;


/* Element */

element :=
  expression ( '..' expression )?
  ;


/*** Terminal Symbols ***/

/* Identifier */

Ident :=
  Letter ( Letter | Digit )*
  ;


/* Decimal Number Literal */

.DecimalNumber :=
   Digit+ RealNumberTail?
  ;

.Digit := '0' .. '9' ;

.RealNumberTail :=
  '.' Digit+ ( 'E' ( '+' | '-' )? Digit+ )?
  ;


/* Prefixed Number Literal */

PrefixedNumberLiteral :=
  '0' ( 'u' | 'x' ) Base16Digit+
  ;

.Base16Digit := Digit | 'A' .. 'F' ;


/* Suffixed Number Literal */

SuffixedNumberLiteral :=
   Base8Digit+ ( 'B' | 'C' ) | Digit Base16Digit* 'H'
  ;

.Base8Digit := '0' .. '7' ;


/* String Literal */

StringLiteral :=
  SingleQuotedString | DoubleQuotedString
  ;


/* Single-Quoted String Literal */

.SingleQuotedString :=
  "'" ( QuotableCharacter | '"' )* "'"
  ;


/* Double-Quoted String Literal */

.DoubleQuotedString :=
  '"' ( QuotableCharacter | "'" )* '"'
  ;


/* Quotable Character */

.QuotableCharacter :=
  Digit | Letter | Space | NonAlphaNumQuotable ;

.Letter := 'a' .. 'z' | 'A' .. 'Z' ;

.Space := 0u20 ;

.NonAlphaNumQuotable :=
  '!' | '#' | '$' | '%' | '&' | '(' | ')' | '*' | '+' | ',' |
  '-' | '.' | '/' | ':' | ';' | '<' | '=' | '>' | '?' | '@' |
  '[' | '\' | ']' | '^' | '_' | '`' | '{' | '|' | '}' | '~'
  ;


/*** Ignore Symbols ***/

/* Whitespace */

Whitespace :=
  Space | Tabulator
  ;

.Tabulator := 0u9 ;


/* Line Comment */

LineComment :=
  '!' AnyPrintable* EndOfLine
  ;

.AnyPrintable := 0u20 .. 0u7E ; /* greedy */


/* Block Comment */

BlockComment :=
  '(*' ( AnyPrintable | BlockComment | EndOfLine )* '*)'
  ;


/* Disabled Code Section */

DisabledCodeSection :=
  '?<' /* strictly in first column of a line */
  ( AnyPrintable | BlockComment | EndOfLine )*
  '>?' /* strictly in first column of a line */
  ;


/* End of Line Marker */

EndOfLine :=
  LF | CR ( LF )?
  ;

.LF := 0uA ;

.CR := 0uD ;

endg Modula2.

/* END OF FILE */