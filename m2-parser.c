/* M2C Modula-2 Compiler & Translator
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * M2C is a compiler and translator for the classic Modula-2 programming
 * language as described in the 3rd and 4th editions of Niklaus Wirth's
 * book "Programming in Modula-2" (PIM) published by Springer Verlag.
 *
 * In compiler mode, M2C compiles Modula-2 source via C to object files or
 * executables using the host system's resident C compiler and linker.
 * In translator mode, it translates Modula-2 source to C source.
 *
 * Further information at http://savannah.nongnu.org/projects/m2c/
 *
 * @file
 *
 * m2-parser.c
 *
 * Implementation of M2C parser module.
 *
 * @license
 *
 * M2C is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation;  either version 2 of the License (GPL2),
 * or (at your option) any later version.
 *
 * M2C is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with m2c.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "m2-parser.h"

#include "m2-lexer.h"
#include "m2-error.h"
#include "m2-tokenset.h"
#include "m2-production.h"
#include "m2-resync-sets.h"
#include "m2-compiler-options.h"

#include <stdio.h>
#include <stdlib.h>


#define PARSER_DEBUG_INFO(_str) \
  {if (m2c_option_parser_debug()) \
    printf("*** %s ***\n  @ line: %u, column: %u, lookahead: %s\n", _str, \
      m2c_lexer_lookahead_line(p->lexer), \
      m2c_lexer_lookahead_column(p->lexer), \
      m2c_string_char_ptr(m2c_lexer_lookahead_lexeme(p->lexer))); }


/* --------------------------------------------------------------------------
 * private type m2c_parser_context_t
 * --------------------------------------------------------------------------
 * Pointer type to represent parser context.
 * ----------------------------------------------------------------------- */

typedef struct m2c_parser_context_s *m2c_parser_context_t;


/* --------------------------------------------------------------------------
 * private type m2c_nonterminal_f
 * --------------------------------------------------------------------------
 * function pointer type for function to parse a non-terminal symbol.
 * ----------------------------------------------------------------------- */

typedef m2c_token_t (m2c_nonterminal_f) (m2c_parser_context_t);


/* --------------------------------------------------------------------------
 * forward declarations of alternative parsing functions.
 * ----------------------------------------------------------------------- */

/* for use with compiler option --variant-records */

m2c_token_t variant_record_type (m2c_parser_context_t p);

/* for use with compiler option --no-variant-records */

m2c_token_t extensible_record_type (m2c_parser_context_t p);


/* --------------------------------------------------------------------------
 * private type m2c_parser_context_s
 * --------------------------------------------------------------------------
 * Record type to implement parser context.
 * ----------------------------------------------------------------------- */

struct m2c_parser_context_s {
  /* lexer */ m2c_lexer_t lexer;
  /* ast_root */ m2c_ast_t ast_root;
  /* error_count */ uint_t error_count;
  /* status */ m2c_parser_status_t status;
  /* record_type */ m2c_nonterminal_f *record_type;
};

typedef struct m2c_parser_context_s m2c_parser_context_s;


/* --------------------------------------------------------------------------
 * procedure m2c_syntax_check_def(filename, status, err_count)
 * --------------------------------------------------------------------------
 * Syntax checks a Modula-2 .DEF file represented by filename.
 * ----------------------------------------------------------------------- */

m2c_token_t definition_module (m2c_parser_context_t p);

void m2c_syntax_check_def
  (m2c_string_t filename, m2c_parser_status_t *status, uint_t *err_count) {
  
  m2c_parser_context_t p;
  m2c_token_t lookahead;
  
  /* set up parser context */
  p = malloc(sizeof(m2c_parser_context_s));
  
  if (p == NULL) {
    SET_STATUS(status, M2C_PARSER_STATUS_ALLOCATION_FAILED);
    
  } /* end if */
  
  /* create lexer object */
  m2c_new_lexer(&(p->lexer), filename, NULL);
  
  if (p->lexer == NULL) {
    SET_STATUS(status, M2C_PARSER_STATUS_ALLOCATION_FAILED);
  } /* end if */
  
  if (m2c_option_variant_records()) {
    /* install function to parse variant records */
    p->record_type = variant_record_type;
  }
  else /* extensible records */ {
    /* install function to parse extensible records */
    p->record_type = extensible_record_type;
  } /* end if */
  
  /* init error counter */
  p->error_count = 0;
  
  /* get the first lookahead symbol */
  lookahead = m2c_next_sym(p->lexer);
  
  /* start parsing */
  if (lookahead == TOKEN_DEFINITION) {
    definition_module(p);
  }
  else /* not a definition part */ {
    /* fatal error */
  } /* end if */
  
  /* pass back error count */
  *err_count = p->error_count;
  
  /* pass back status and return */
  SET_STATUS(status, p->status);
  return;
} /* end m2c_syntax_check_def */


/* --------------------------------------------------------------------------
 * procedure m2c_syntax_check_mod(filename, status, err_count)
 * --------------------------------------------------------------------------
 * Syntax checks a Modula-2 .MOD file represented by filename.
 * ----------------------------------------------------------------------- */

m2c_token_t implementation_module (m2c_parser_context_t p);

m2c_token_t program_module (m2c_parser_context_t p);

void m2c_syntax_check_mod
  (m2c_string_t filename, m2c_parser_status_t *status, uint_t *err_count) {
  
  m2c_parser_context_t p;
  m2c_token_t lookahead;
  
  /* set up parser context */
  p = malloc(sizeof(m2c_parser_context_s));
  
  if (p == NULL) {
    SET_STATUS(status, M2C_PARSER_STATUS_ALLOCATION_FAILED);
    
  } /* end if */
  
  /* create lexer object */
  m2c_new_lexer(&(p->lexer), filename, NULL);
  
  if (p->lexer == NULL) {
    SET_STATUS(status, M2C_PARSER_STATUS_ALLOCATION_FAILED);
  } /* end if */
  
  if (m2c_option_variant_records()) {
    /* install function to parse variant records */
    p->record_type = variant_record_type;
  }
  else /* extensible records */ {
    /* install function to parse extensible records */
    p->record_type = extensible_record_type;
  } /* end if */
  
  /* init error counter */
  p->error_count = 0;
  
  /* get the first lookahead symbol */
  lookahead = m2c_next_sym(p->lexer);
  
  /* start parsing */
  if (lookahead == TOKEN_IMPLEMENTATION) {
    implementation_module(p);
  }
  else if (lookahead == TOKEN_MODULE) {
    program_module(p);
  }
  else /* not a program or implementation part */ {
    /* fatal error */
  } /* end if */
  
  /* pass back error count */
  *err_count = p->error_count;
  
  /* pass back status and return */
  SET_STATUS(status, p->status);
  return;
} /* end m2c_syntax_check_mod */


/* --------------------------------------------------------------------------
 * function m2c_parse_def(filename, status)
 * --------------------------------------------------------------------------
 * Parses a Modula-2 .DEF file represented by filename and returns an AST.
 * ----------------------------------------------------------------------- */

m2c_ast_t m2c_parse_def
  (m2c_string_t filename, m2c_parser_status_t *status) {
  
  /* TO DO */
  
  return NULL;
} /* end m2c_parse_def */


/* --------------------------------------------------------------------------
 * function m2c_parse_def(filename, status)
 * --------------------------------------------------------------------------
 * Parses a Modula-2 .MOD file represented by filename and returns an AST.
 * ----------------------------------------------------------------------- */

m2c_ast_t m2c_parse_mod
  (m2c_string_t filename, m2c_parser_status_t *status) {
  
  /* TO DO */
  
  return NULL;
} /* end m2c_parse_mod */


/* --------------------------------------------------------------------------
 * private function match_token(p, expected_token, resync_set)
 * --------------------------------------------------------------------------
 * Matches the lookahead symbol to expected_token and returns true if they
 * match.  If they don't match, a syntax error is reported, the error count
 * is incremented, symbols are consumed until the lookahead symbol matches
 * one of the symbols in resync_set and false is returned.
 * ----------------------------------------------------------------------- */

bool match_token
  (m2c_parser_context_t p,
   m2c_token_t expected_token,
   m2c_tokenset_t resync_set) {
  
  m2c_string_t lexeme;
  const char *lexstr;
  uint_t line, column;
  m2c_token_t lookahead;
  
  lookahead = m2c_next_sym(p->lexer);
  
  if (lookahead == expected_token) {
    return true;
  }
  else /* no match */ {
    /* get details for offending lookahead symbol */
    line = m2c_lexer_lookahead_line(p->lexer);
    column = m2c_lexer_lookahead_column(p->lexer);
    lexeme = m2c_lexer_lookahead_lexeme(p->lexer);
    lexstr = m2c_string_char_ptr(lexeme);
    
    /* report error */
    m2c_emit_syntax_error_w_token
      (line, column, lookahead, lexstr, expected_token);
    
    /* print source line */
    if (m2c_option_verbose()) {
      m2c_print_line_and_mark_column(p->lexer, line, column);
    } /* end if */
    
    /* update error count */
    p->error_count++;
    
    /* skip symbols until lookahead matches resync_set */
    while (!m2c_tokenset_element(resync_set, lookahead)) {
      lookahead = m2c_consume_sym(p->lexer);
    } /* end while */
    return false;
  } /* end if */
} /* end match_token */


/* --------------------------------------------------------------------------
 * private function match_set(p, expected_set, resync_set)
 * --------------------------------------------------------------------------
 * Matches the lookahead symbol to set expected_set and returns true if it
 * matches any of the tokens in the set.  If there is no match, a syntax
 * error is reported, the error count is incremented, symbols are consumed
 * until the lookahead symbol matches one of the symbols in resync_set and
 * false is returned.
 * ----------------------------------------------------------------------- */

bool match_set
  (m2c_parser_context_t p,
   m2c_tokenset_t expected_set,
   m2c_tokenset_t resync_set) {
  
  m2c_string_t lexeme;
  const char *lexstr;
  uint_t line, column;
  m2c_token_t lookahead;
  
  lookahead = m2c_next_sym(p->lexer);
  
  /* check if lookahead matches any token in expected_set */
  if (m2c_tokenset_element(expected_set, lookahead)) {
    return true;
  }
  else /* no match */ {
    /* get details for offending lookahead symbol */
    line = m2c_lexer_lookahead_line(p->lexer);
    column = m2c_lexer_lookahead_column(p->lexer);
    lexeme = m2c_lexer_lookahead_lexeme(p->lexer);
    lexstr = m2c_string_char_ptr(lexeme);
    
    /* report error */
    m2c_emit_syntax_error_w_set
      (line, column, lookahead, lexstr, expected_set);
    
    /* print source line */
    if (m2c_option_verbose()) {
      m2c_print_line_and_mark_column(p->lexer, line, column);
    } /* end if */
        
    /* update error count */
    p->error_count++;
    
    /* skip symbols until lookahead matches resync_set */
    while (!m2c_tokenset_element(resync_set, lookahead)) {
      lookahead = m2c_consume_sym(p->lexer);
    } /* end while */
    return false;
  } /* end if */
} /* end match_set */


/* ************************************************************************ *
 * Definition Module Syntax                                                 *
 * ************************************************************************ */


/* --------------------------------------------------------------------------
 * private procedure definition_module()
 * --------------------------------------------------------------------------
 * definitionModule :=
 *   DEFINITION MODULE moduleIdent ';'
 *   import* definition* END moduleIdent '.'
 *   ;
 *
 * moduleIdent := Ident ;
 * ----------------------------------------------------------------------- */

m2c_token_t import (m2c_parser_context_t p);

m2c_token_t definition (m2c_parser_context_t p);

m2c_token_t definition_module (m2c_parser_context_t p) {
  m2c_string_t module_ident_header, module_ident_end;
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("definitionModule");
  
  /* DEFINITION */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* MODULE */
  if (match_token(p, TOKEN_MODULE, RESYNC(IMPORT_OR_DEFINITON_OR_END))) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* moduleIdent */
    if (match_token(p, TOKEN_IDENTIFIER,
        RESYNC(IMPORT_OR_DEFINITON_OR_END))) {
      lookahead = m2c_consume_sym(p->lexer);
      module_ident_header = m2c_lexer_current_lexeme(p->lexer);
      
      /* ';' */
      if (match_token(p, TOKEN_SEMICOLON,
          RESYNC(IMPORT_OR_DEFINITON_OR_END))) {
        lookahead = m2c_consume_sym(p->lexer);
      }
      else /* resync */ {
        lookahead = m2c_next_sym(p->lexer);
      } /* end if */
    }
    else /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
  }
  else /* resync */ {
    lookahead = m2c_next_sym(p->lexer);
  } /* end if */
  
  /* import* */
  while ((lookahead == TOKEN_IMPORT) ||
         (lookahead == TOKEN_FROM)) {
    lookahead = import(p);
  } /* end while */
  
  /* definition* */
  while ((lookahead == TOKEN_CONST) ||
         (lookahead == TOKEN_TYPE) ||
         (lookahead == TOKEN_VAR) ||
         (lookahead == TOKEN_PROCEDURE)) {
    lookahead = definition(p);
  } /* end while */
  
  /* END */
  if (match_token(p, TOKEN_END, FOLLOW(DEFINITION_MODULE))) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* moduleIdent */
    if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(DEFINITION_MODULE))) {
      lookahead = m2c_consume_sym(p->lexer);
      module_ident_end = m2c_lexer_current_lexeme(p->lexer);
    
      if (module_ident_header != module_ident_end) {
        /* error: module identifiers don't match */ 
      } /* end if */
    
      /* '.' */
      if (match_token(p, TOKEN_PERIOD, FOLLOW(DEFINITION_MODULE))) {
        lookahead = m2c_consume_sym(p->lexer);
      } /* end if */
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end definition_module */


/* --------------------------------------------------------------------------
 * private procedure import()
 * --------------------------------------------------------------------------
 * import :=
 *   ( qualifiedImport | unqualifiedImport ) ';'
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t qualified_import (m2c_parser_context_t p);

m2c_token_t unqualified_import (m2c_parser_context_t p);

m2c_token_t import (m2c_parser_context_t p) {
  
  m2c_token_t lookahead = m2c_next_sym(p->lexer);
  
  PARSER_DEBUG_INFO("import");
  
  lookahead = m2c_next_sym(p->lexer);
  
  /* qualifiedImport */
  if (lookahead == TOKEN_IMPORT) {
    lookahead = qualified_import(p);
  }
  /* | unqualifiedImport */
  else if (lookahead == TOKEN_FROM) {
    lookahead = unqualified_import(p);
  }
  else /* unreachable code */ {
    /* fatal error -- abort */
  } /* end if */
  
  /* ';' */
  if (match_token(p, TOKEN_SEMICOLON, RESYNC(IMPORT_OR_DEFINITON_OR_END))) {
    lookahead = m2c_consume_sym(p->lexer);
  } /* end if */
  
  return lookahead;
} /* end import */


/* --------------------------------------------------------------------------
 * private procedure qualified_import()
 * --------------------------------------------------------------------------
 * qualifiedImport :=
 *   IMPORT moduleList
 *   ;
 *
 * moduleList := identList ;
 * ----------------------------------------------------------------------- */

m2c_token_t ident_list (m2c_parser_context_t p);

m2c_token_t qualified_import (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("qualifiedImport");
  
  /* IMPORT */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* moduleList */
  if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(QUALIFIED_IMPORT))) {
    lookahead = ident_list(p);
  } /* end if */

  return lookahead;
} /* end qualified_import */


/* --------------------------------------------------------------------------
 * private procedure unqualified_import()
 * --------------------------------------------------------------------------
 * unqualifiedImport :=
 *   FROM moduleIdent IMPORT identList
 *   ;
 *
 * moduleIdent := Ident ;
 * ----------------------------------------------------------------------- */

m2c_token_t unqualified_import (m2c_parser_context_t p) {
  m2c_string_t module_ident;
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("unqualifiedImport");
  
  /* FROM */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* moduleIdent */
  if (match_token(p, TOKEN_IDENTIFIER,
      RESYNC(IMPORT_OR_IDENT_OR_SEMICOLON))) {
    lookahead = m2c_consume_sym(p->lexer);
    module_ident = m2c_lexer_current_lexeme(p->lexer);

    /* IMPORT */
    if (match_token(p, TOKEN_IMPORT, RESYNC(IDENT_OR_SEMICOLON))) {
      lookahead = m2c_consume_sym(p->lexer);
      
      /* moduleList */
      if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(UNQUALIFIED_IMPORT))) {
        lookahead = ident_list(p);
      } /* end if */
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end unqualified_import */


/* --------------------------------------------------------------------------
 * private procedure ident_list()
 * --------------------------------------------------------------------------
 * identList :=
 *   Ident ( ',' Ident )*
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t ident_list (m2c_parser_context_t p) {
  m2c_string_t ident;
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("identList");
  
  /* Ident */
  lookahead = m2c_consume_sym(p->lexer);
  ident = m2c_lexer_current_lexeme(p->lexer);
  /* TO DO: add ident to list structure */
  
  /* ( ',' Ident )* */
  while (lookahead == TOKEN_COMMA) {
    /* ',' */
    lookahead = m2c_consume_sym(p->lexer);
    
    /* Ident */
    if (match_token(p, TOKEN_IDENTIFIER, RESYNC(COMMA_OR_SEMICOLON))) {
      lookahead = m2c_consume_sym(p->lexer);
      ident = m2c_lexer_current_lexeme(p->lexer);
      /* TO DO: add ident to list structure */
    } /* end if */
  } /* end while */
  
  return lookahead;
} /* end ident_list */


/* --------------------------------------------------------------------------
 * private procedure definition()
 * --------------------------------------------------------------------------
 * definition :=
 *   CONST ( constDefinition ';' )* |
 *   TYPE ( typeDefinition ';' )* |
 *   VAR ( varDefinition ';' )* |
 *   procedureHeader ';'
 *   ;
 *
 * varDefinition := variableDeclaration ;
 * ----------------------------------------------------------------------- */

m2c_token_t const_definition (m2c_parser_context_t p);

m2c_token_t type_definition (m2c_parser_context_t p);

m2c_token_t variable_declaration (m2c_parser_context_t p);

m2c_token_t procedure_header (m2c_parser_context_t p);

m2c_token_t definition (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("definition");
  
  lookahead = m2c_next_sym(p->lexer);
  
  switch (lookahead) {
    
    /* CONST */
    case TOKEN_CONST :
      lookahead = m2c_consume_sym(p->lexer);
      
      /* ( constDefinition ';' )* */
      while (lookahead == TOKEN_IDENTIFIER) {
        lookahead = const_definition(p);
        
        /* ';' */
        if (match_token(p, TOKEN_SEMICOLON,
            RESYNC(DEFINITION_OR_IDENT_OR_SEMICOLON))) {
          lookahead = m2c_consume_sym(p->lexer);
        } /* end if */
      } /* end while */
      break;
      
    /* | TYPE */
    case TOKEN_TYPE :
      lookahead = m2c_consume_sym(p->lexer);
      
      /* ( typeDefinition ';' )* */
      while (lookahead == TOKEN_IDENTIFIER) {
        lookahead = type_definition(p);
        
        /* ';' */
        if (match_token(p, TOKEN_SEMICOLON,
            RESYNC(DEFINITION_OR_IDENT_OR_SEMICOLON))) {
          lookahead = m2c_consume_sym(p->lexer);
        } /* end if */
      } /* end while */
      break;
      
    /* | VAR */
    case TOKEN_VAR :
      lookahead = m2c_consume_sym(p->lexer);
      
      /* ( varDefinition ';' )* */
      while (lookahead == TOKEN_IDENTIFIER) {
        lookahead = variable_declaration(p);
        
        /* ';' */
        if (match_token(p, TOKEN_SEMICOLON,
            RESYNC(DEFINITION_OR_IDENT_OR_SEMICOLON))) {
          lookahead = m2c_consume_sym(p->lexer);
        } /* end if */
      } /* end while */
      break;
      
    /* | procedureHeader */
    case TOKEN_PROCEDURE :
      lookahead = procedure_header(p);
      
      /* ';' */
      if (match_token(p, TOKEN_SEMICOLON,
          RESYNC(DEFINITION_OR_SEMICOLON))) {
        lookahead = m2c_consume_sym(p->lexer);
      } /* end if */
      break;
      
    default : /* unreachable code */
      /* fatal error -- abort */
      exit(-1);
  } /* end switch */
  
  return lookahead;
} /* end definition */


/* --------------------------------------------------------------------------
 * private procedure const_definition()
 * --------------------------------------------------------------------------
 * constDefinition :=
 *   Ident '=' constExpression
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t const_expression (m2c_parser_context_t p);

m2c_token_t const_definition (m2c_parser_context_t p) {
  m2c_string_t ident;
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("constDefinition");
  
  /* Ident */
  lookahead = m2c_consume_sym(p->lexer);
  ident = m2c_lexer_current_lexeme(p->lexer);
  
  /* '=' */
  if (match_token(p, TOKEN_EQUAL, FOLLOW(CONST_DEFINITION))) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* constExpression */
    if (match_set(p, FIRST(EXPRESSION), FOLLOW(CONST_DEFINITION))) {
      lookahead = const_expression(p);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end const_definition */


/* --------------------------------------------------------------------------
 * private procedure type_definition()
 * --------------------------------------------------------------------------
 * typeDefinition :=
 *   Ident ( '=' type )?
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t type (m2c_parser_context_t p);

m2c_token_t type_definition (m2c_parser_context_t p) {
  m2c_string_t ident;
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("typeDefinition");
  
  /* Ident */
  lookahead = m2c_consume_sym(p->lexer);
  ident = m2c_lexer_current_lexeme(p->lexer);
  
  /* ( '=' type )? */
  if (lookahead == TOKEN_EQUAL) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* type */
    if (match_set(p, FIRST(TYPE), FOLLOW(TYPE_DEFINITION))) {
      lookahead = type(p);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end type_definition */


/* --------------------------------------------------------------------------
 * private procedure type()
 * --------------------------------------------------------------------------
 * type :=
 *   derivedOrSubrangeType | enumType | setType | arrayType |
 *   recordType | pointerType | procedureType
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t derived_or_subrange_type (m2c_parser_context_t p);

m2c_token_t enum_type (m2c_parser_context_t p);

m2c_token_t set_type (m2c_parser_context_t p);

m2c_token_t array_type (m2c_parser_context_t p);

m2c_token_t pointer_type (m2c_parser_context_t p);

m2c_token_t procedure_type (m2c_parser_context_t p);

m2c_token_t type (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("type");
  
  lookahead = m2c_next_sym(p->lexer);
  
  switch (lookahead) {
  
    /* derivedOrSubrangeType */
    case TOKEN_IDENTIFIER :
    case TOKEN_LEFT_BRACKET :
      lookahead = derived_or_subrange_type(p);
      break;
      
    /* | enumType */
    case TOKEN_LEFT_PAREN :
      lookahead = enum_type(p);
      break;
      
    /* | setType */
    case TOKEN_SET :
      lookahead = set_type(p);
      break;
      
    /* | arrayType */
    case TOKEN_ARRAY :
      lookahead = array_type(p);
      break;
      
    /* | recordType */
    case TOKEN_RECORD :
      lookahead = p->record_type(p);
      break;
      
    /* | pointerType */
    case TOKEN_POINTER :
      lookahead = pointer_type(p);
      break;
      
    /* | procedureType */
    case TOKEN_PROCEDURE :
      lookahead = procedure_type(p);
      break;
      
    default : /* unreachable code */
      /* fatal error -- abort */
      exit(-1);
    } /* end switch */
  
  return lookahead;
} /* end type */


/* --------------------------------------------------------------------------
 * private procedure derived_or_subrange_type()
 * --------------------------------------------------------------------------
 * derivedOrSubrangeType :=
 *   typeIdent range? | range
 *   ;
 *
 * typeIdent := qualident ;
 * ----------------------------------------------------------------------- */

m2c_token_t qualident (m2c_parser_context_t p);

m2c_token_t range (m2c_parser_context_t p);

m2c_token_t derived_or_subrange_type (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("derivedOrSubrangeType");
  
  lookahead = m2c_next_sym(p->lexer);
  
  if (match_set(p, FIRST(DERIVED_OR_SUBRANGE_TYPE),
      FOLLOW(DERIVED_OR_SUBRANGE_TYPE))) {
    
    /* typeIdent range? */
    if (lookahead == TOKEN_IDENTIFIER) {
      
      /* typeIdent */
      lookahead = qualident(p);
      
      /* range? */
      if (lookahead == TOKEN_LEFT_BRACKET) {
        lookahead = range(p);
      } /* end if */
    }
    /* | range */
    else if (lookahead == TOKEN_LEFT_BRACKET) {
      lookahead = range(p);
    }
    else /* unreachable code */ {
      /* fatal error -- abort */
      exit(-1);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end derived_or_subrange_type */


/* --------------------------------------------------------------------------
 * private procedure qualident()
 * --------------------------------------------------------------------------
 * qualident :=
 *   Ident ( '.' Ident )*
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t qualident (m2c_parser_context_t p) {
  m2c_string_t ident;
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("qualident");
  
  /* Ident */
  lookahead = m2c_consume_sym(p->lexer);
  ident = m2c_lexer_current_lexeme(p->lexer);
  /* TO DO: add ident to list structure */
  
  /* ( '.' Ident )* */
  while (lookahead == TOKEN_PERIOD) {
    /* '.' */
    lookahead = m2c_consume_sym(p->lexer);
    
    /* Ident */
    if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(QUALIDENT))) {
      lookahead = m2c_consume_sym(p->lexer);
      ident = m2c_lexer_current_lexeme(p->lexer);
      /* TO DO: add ident to list structure */
    } /* end if */
  } /* end while */
  
  return lookahead;
} /* end qualident */


/* --------------------------------------------------------------------------
 * private procedure range()
 * --------------------------------------------------------------------------
 * range :=
 *   '[' constExpression '..' constExpression ']'
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t range (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("range");
  
  /* '[' */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* constExpression */
  if (match_set(p, FIRST(EXPRESSION), FOLLOW(RANGE))) {
    lookahead = const_expression(p);
    
    /* '..' */
    if (match_token(p, TOKEN_RANGE, FOLLOW(RANGE))) {
      lookahead = m2c_consume_sym(p->lexer);
      
      /* constExpression */
      if (match_set(p, FIRST(EXPRESSION), FOLLOW(RANGE))) {
        lookahead = const_expression(p);
        
        /* ']' */
        if (match_token(p, TOKEN_RIGHT_BRACKET, FOLLOW(RANGE))) {
          lookahead = m2c_consume_sym(p->lexer);
        } /* end if */
      } /* end if */
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end range */


/* --------------------------------------------------------------------------
 * private procedure enum_type()
 * --------------------------------------------------------------------------
 * enumType :=
 *   '(' identList ')'
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t enum_type (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("enumType");
  
  /* '(' */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* identList */
  if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(ENUM_TYPE))) {
    lookahead = ident_list(p);
    
    /* ')' */
    if (match_token(p, TOKEN_RIGHT_PAREN, FOLLOW(ENUM_TYPE))) {
      lookahead = m2c_consume_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end enum_type */


/* --------------------------------------------------------------------------
 * private procedure set_type()
 * --------------------------------------------------------------------------
 * setType :=
 *   SET OF countableType
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t countable_type (m2c_parser_context_t p);

m2c_token_t set_type (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("setType");
  
  /* SET */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* OF */
  if (match_token(p, TOKEN_OF, FOLLOW(SET_TYPE))) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* countableType */
    if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(SET_TYPE))) {
      lookahead = countable_type(p);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end set_type */


/* --------------------------------------------------------------------------
 * private procedure countable_type()
 * --------------------------------------------------------------------------
 * countableType :=
 *   range | enumType | countableTypeIdent range?
 *   ;
 *
 * countableTypeIdent := typeIdent ;
 * ----------------------------------------------------------------------- */

m2c_token_t countable_type (m2c_parser_context_t p) {
  m2c_string_t ident;
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("countableType");
  
  lookahead = m2c_next_sym(p->lexer);
  
  switch (lookahead) {
  
    /* range */
    case TOKEN_LEFT_BRACKET :
      lookahead = range(p);
      break;
      
    /* | enumType */
    case TOKEN_LEFT_PAREN :
      lookahead = enum_type(p);
      break;
      
    /* | countableTypeIdent range? */
    case TOKEN_IDENTIFIER :
      lookahead = m2c_consume_sym(p->lexer);
      ident = m2c_lexer_current_lexeme(p->lexer);
      
      /* range? */
      if (lookahead == TOKEN_LEFT_BRACKET) {
        lookahead = range(p);
      } /* end if */
      break;
      
    default : /* unreachable code */
      /* fatal error -- abort */
      exit(-1);
    } /* end switch */
  
  return lookahead;
} /* end countable_type */


/* --------------------------------------------------------------------------
 * private procedure array_type()
 * --------------------------------------------------------------------------
 * arrayType :=
 *   ARRAY countableType ( ',' countableType )* OF type
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t array_type (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("arrayType");
  
  /* ARRAY */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* countableType */
  if (match_set(p, FIRST(COUNTABLE_TYPE), FOLLOW(ARRAY_TYPE))) {
    lookahead = countable_type(p);
    
    /* ( ',' countableType )* */
    while (lookahead == TOKEN_COMMA) {
      /* ',' */
      lookahead = m2c_consume_sym(p->lexer);
      
      if (match_set(p, FIRST(COUNTABLE_TYPE), RESYNC(TYPE_OR_COMMA_OR_OF))) {
        lookahead = countable_type(p);
      }
      else /* resync */ {
        lookahead = m2c_next_sym(p->lexer);
      } /* end if */
    } /* end while */

    /* OF */
    if (match_token(p, TOKEN_OF, FOLLOW(ARRAY_TYPE))) {
      lookahead = m2c_consume_sym(p->lexer);
  
      /* type */
      if (match_set(p, FIRST(TYPE), FOLLOW(ARRAY_TYPE))) {
        lookahead = type(p);
      } /* end if */
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end array_type */


/* --------------------------------------------------------------------------
 * private procedure extensible_record_type()
 * --------------------------------------------------------------------------
 * For use with compiler option --no-variant-records.
 *
 * recordType := extensibleRecordType ;
 *
 * extensibleRecordType :=
 *   RECORD ( '(' baseType ')' )? fieldListSequence END
 *   ;
 *
 * baseType := typeIdent ;
 * ----------------------------------------------------------------------- */

m2c_token_t field_list_sequence (m2c_parser_context_t p);

m2c_token_t extensible_record_type (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("recordType");
  
  /* RECORD */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* ( '(' baseType ')' )? */
  if (lookahead == TOKEN_LEFT_PAREN) {
    /* '(' */
    lookahead = m2c_consume_sym(p->lexer);
    
    /* baseType */
    if (match_token(p, TOKEN_IDENTIFIER,
        FIRST(FIELD_LIST_SEQUENCE))) {
      lookahead = qualident(p);
      
      /* ')' */
      if (match_token(p, TOKEN_RIGHT_PAREN,
          FIRST(FIELD_LIST_SEQUENCE))) {
        lookahead = m2c_consume_sym(p->lexer);
      }
      else /* resync */ {
        lookahead = m2c_next_sym(p->lexer);
      } /* end if */
    }
    else  /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  /* check for empty field list sequence */
  if (lookahead == TOKEN_END) {

      /* empty field list sequence warning */
      m2c_emit_warning_w_pos
        (M2C_EMPTY_FIELD_LIST_SEQ,
         m2c_lexer_lookahead_line(p->lexer),
         m2c_lexer_lookahead_column(p->lexer));
      
      /* END */
      lookahead = m2c_consume_sym(p->lexer);
  }
  
  /* fieldListSequence */
  else if (match_set(p, FIRST(FIELD_LIST_SEQUENCE),
           FOLLOW(EXTENSIBLE_RECORD_TYPE))) {
    lookahead = field_list_sequence(p);
    
    /* END */
    if (match_token(p, TOKEN_END, FOLLOW(EXTENSIBLE_RECORD_TYPE))) {
      lookahead = m2c_consume_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end extensible_record_type */


/* --------------------------------------------------------------------------
 * private procedure field_list_sequence()
 * --------------------------------------------------------------------------
 * For use with compiler option --no-variant-records.
 *
 * fieldListSequence :=
 *   fieldList ( ';' fieldList )*
 *   ;
 *
 * fieldList := variableDeclaration ;
 * ----------------------------------------------------------------------- */

m2c_token_t variable_declaration (m2c_parser_context_t p);

m2c_token_t field_list_sequence (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  uint_t line_of_semicolon, column_of_semicolon;
  
  PARSER_DEBUG_INFO("fieldListSequence");
  
  /* fieldList */
  lookahead = variable_declaration(p);
  
  /* ( ';' fieldList )* */
  while (lookahead == TOKEN_SEMICOLON) {
    /* ';' */
    line_of_semicolon = m2c_lexer_lookahead_line(p->lexer);
    column_of_semicolon = m2c_lexer_lookahead_column(p->lexer);
    lookahead = m2c_consume_sym(p->lexer);
    
    /* check if semicolon occurred at the end of a field list sequence */
    if (m2c_tokenset_element(FOLLOW(FIELD_LIST_SEQUENCE), lookahead)) {
    
      if (m2c_option_errant_semicolon()) {
        /* treat as warning */
        m2c_emit_warning_w_pos
          (M2C_SEMICOLON_AFTER_FIELD_LIST_SEQ,
           line_of_semicolon, column_of_semicolon);
      }
      else /* treat as error */ {
        m2c_emit_error_w_pos
          (M2C_SEMICOLON_AFTER_FIELD_LIST_SEQ,
           line_of_semicolon, column_of_semicolon);
        p->error_count++;
      } /* end if */
      
      /* print source line */
      if (m2c_option_verbose()) {
        m2c_print_line_and_mark_column(p->lexer,
          line_of_semicolon, column_of_semicolon);
      } /* end if */
    
      /* leave field list sequence loop to continue */
      break;
    } /* end if */
    
    /* fieldList */
    if (match_set(p, FIRST(VARIABLE_DECLARATION),
        RESYNC(SEMICOLON_OR_END))) {
      lookahead = variable_declaration(p);
    } /* end if */
  } /* end while */
  
  return lookahead;
} /* end field_list_sequence */


/* --------------------------------------------------------------------------
 * private procedure variant_record_type()
 * --------------------------------------------------------------------------
 * For use with compiler option --variant-records.
 *
 * recordType := variantRecordType ;
 *
 * variantRecordType :=
 *   RECORD variantFieldListSeq END
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t variant_field_list_seq (m2c_parser_context_t p);

m2c_token_t variant_record_type (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("recordType");
  
  /* RECORD */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* check for empty field list sequence */
  if (lookahead == TOKEN_END) {

      /* empty field list sequence warning */
      m2c_emit_warning_w_pos
        (M2C_EMPTY_FIELD_LIST_SEQ,
         m2c_lexer_lookahead_line(p->lexer),
         m2c_lexer_lookahead_column(p->lexer));
      
      /* END */
      lookahead = m2c_consume_sym(p->lexer);
  }
  
  /* variantFieldListSeq */
  else if(match_set(p, FIRST(VARIANT_FIELD_LIST_SEQ),
          FOLLOW(VARIANT_RECORD_TYPE))) {
    lookahead = variant_field_list_seq(p);
    
    /* END */
    if (match_token(p, TOKEN_END, FOLLOW(VARIANT_RECORD_TYPE))) {
      lookahead = m2c_consume_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end variant_record_type */


/* --------------------------------------------------------------------------
 * private procedure variant_field_list_seq()
 * --------------------------------------------------------------------------
 * For use with compiler option --variant-records.
 *
 * variantFieldListSeq :=
 *   variantFieldList ( ';' variantFieldList )*
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t variant_field_list (m2c_parser_context_t p);

m2c_token_t variant_field_list_seq (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  uint_t line_of_semicolon, column_of_semicolon;
  
  PARSER_DEBUG_INFO("variantFieldListSeq");
  
  /* variantFieldList */
  lookahead = variant_field_list(p);
  
  /* ( ';' variantFieldList )* */
  while (lookahead == TOKEN_SEMICOLON) {
    /* ';' */
    line_of_semicolon = m2c_lexer_lookahead_line(p->lexer);
    column_of_semicolon = m2c_lexer_lookahead_column(p->lexer);
    lookahead = m2c_consume_sym(p->lexer);
    
    /* check if semicolon occurred at the end of a field list sequence */
    if (m2c_tokenset_element(FOLLOW(VARIANT_FIELD_LIST_SEQ), lookahead)) {
    
      if (m2c_option_errant_semicolon()) {
        /* treat as warning */
        m2c_emit_warning_w_pos
          (M2C_SEMICOLON_AFTER_FIELD_LIST_SEQ,
           line_of_semicolon, column_of_semicolon);
      }
      else /* treat as error */ {
        m2c_emit_error_w_pos
          (M2C_SEMICOLON_AFTER_FIELD_LIST_SEQ,
           line_of_semicolon, column_of_semicolon);
        p->error_count++;
      } /* end if */
      
      /* print source line */
      if (m2c_option_verbose()) {
        m2c_print_line_and_mark_column(p->lexer,
          line_of_semicolon, column_of_semicolon);
      } /* end if */
    
      /* leave field list sequence loop to continue */
      break;
    } /* end if */
    
    /* variantFieldList */
    if (match_set(p, FIRST(VARIANT_FIELD_LIST),
        FOLLOW(VARIANT_FIELD_LIST))) {
      lookahead = variant_field_list(p);
    } /* end if */
  } /* end while */
  
  return lookahead;
} /* end variant_field_list_seq */


/* --------------------------------------------------------------------------
 * private procedure variant_field_list()
 * --------------------------------------------------------------------------
 * For use with compiler option --variant-records.
 *
 * variantFieldList :=
 *   fieldList | variantFields
 *   ;
 *
 * fieldList := variableDeclaration ;
 * ----------------------------------------------------------------------- */

m2c_token_t variant_fields (m2c_parser_context_t p);

m2c_token_t variant_field_list (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("variantFieldList");
  
  lookahead = m2c_next_sym(p->lexer);
  
  /* fieldList */
  if (lookahead == TOKEN_IDENTIFIER) {
    lookahead = variable_declaration(p);
  }
  /* | variantFields */
  else if (lookahead == TOKEN_CASE) {
    lookahead = variant_fields(p);
  }
  else /* unreachable code */ {
    /* fatal error -- abort */
      exit(-1);
  } /* end if */
  
  return lookahead;
} /* end variant_field_list */


/* --------------------------------------------------------------------------
 * private procedure variant_fields()
 * --------------------------------------------------------------------------
 * For use with compiler option --variant-records.
 *
 * variantFields :=
 *   CASE Ident? ':' typeIdent OF
 *   variant ( '|' variant )*
 *   ( ELSE fieldListSequence )?
 *   END
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t variant (m2c_parser_context_t p);

m2c_token_t variant_fields (m2c_parser_context_t p) {
  m2c_string_t ident, type_ident;
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("variantFields");
  
  /* CASE */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* Ident? */
  if (lookahead == TOKEN_IDENTIFIER) {
    lookahead = m2c_consume_sym(p->lexer);
    ident = m2c_lexer_current_lexeme(p->lexer);
  } /* end if */
  
  /* ':' */
  if (match_token(p, TOKEN_COLON, RESYNC(ELSE_OR_END))) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* typeIdent */
    if (match_token(p, TOKEN_IDENTIFIER, RESYNC(ELSE_OR_END))) {
      lookahead = m2c_consume_sym(p->lexer);
      type_ident = m2c_lexer_current_lexeme(p->lexer);
    
      /* OF */
      if (match_token(p, TOKEN_OF, RESYNC(ELSE_OR_END))) {
        lookahead = m2c_consume_sym(p->lexer);
      
        /* variant */
        if (match_set(p, FIRST(VARIANT), RESYNC(ELSE_OR_END))) {
          lookahead = variant(p);
        
          /* ( '|' variant )* */
          while (lookahead == TOKEN_BAR) {
          
            /* '|' */
            lookahead = m2c_consume_sym(p->lexer);
          
            /* variant */
            if (match_set(p, FIRST(VARIANT), RESYNC(ELSE_OR_END))) {
              lookahead = variant(p);
            } /* end if */
          } /* end while */
        } /* end if */
      } /* end if */
    } /* end if */
  } /* end if */
  
  /* resync */
  lookahead = m2c_next_sym(p->lexer);
    
  /* ( ELSE fieldListSequence )? */
  if (lookahead == TOKEN_ELSE) {
  
    /* ELSE */
    lookahead = m2c_consume_sym(p->lexer);
  
    /* check for empty field list sequence */
    if (lookahead == TOKEN_END) {

        /* empty field list sequence warning */
        m2c_emit_warning_w_pos
          (M2C_EMPTY_FIELD_LIST_SEQ,
           m2c_lexer_lookahead_line(p->lexer),
           m2c_lexer_lookahead_column(p->lexer));
    }
  
    /* fieldListSequence */
    else if (match_set(p,
             FIRST(FIELD_LIST_SEQUENCE), FOLLOW(VARIANT_FIELDS))) {
      lookahead = field_list_sequence(p);
    }
    else /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  /* END */
  if (match_token(p, TOKEN_END, FOLLOW(VARIANT_FIELDS))) {
    lookahead = m2c_consume_sym(p->lexer);
  } /* end if */
  
  return lookahead;
} /* end variant_fields */


/* --------------------------------------------------------------------------
 * private procedure variant()
 * --------------------------------------------------------------------------
 * For use with compiler option --variant-records.
 *
 * variant :=
 *   caseLabelList ':' variantFieldListSeq
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t case_label_list (m2c_parser_context_t p);

m2c_token_t variant (m2c_parser_context_t p) {
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("variant");
  
  /* caseLabelList */
  lookahead = case_label_list(p);
  
  /* ':' */
  if (match_token(p, TOKEN_COLON, FOLLOW(VARIANT))) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* check for empty field list sequence */
    if (m2c_tokenset_element(FOLLOW(VARIANT), lookahead)) {

        /* empty field list sequence warning */
        m2c_emit_warning_w_pos
          (M2C_EMPTY_FIELD_LIST_SEQ,
           m2c_lexer_lookahead_line(p->lexer),
           m2c_lexer_lookahead_column(p->lexer));
    }
  
    /* variantFieldListSeq */
    else if (match_set(p, FIRST(VARIANT_FIELD_LIST_SEQ), FOLLOW(VARIANT))) {
      lookahead = variant_field_list_seq(p);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end variant */


/* --------------------------------------------------------------------------
 * private procedure case_label_list()
 * --------------------------------------------------------------------------
 * caseLabelList :=
 *   caseLabels ( ',' caseLabels )*
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t case_labels (m2c_parser_context_t p);

m2c_token_t case_label_list (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("caseLabelList");
  
  /* caseLabels */
  lookahead = case_labels(p);
  
  /* ( ',' caseLabels )* */
  while (lookahead == TOKEN_COMMA) {
    /* ',' */
    lookahead = m2c_consume_sym(p->lexer);
    
    /* caseLabels */
    if (match_set(p, FIRST(CASE_LABELS), FOLLOW(CASE_LABEL_LIST))) {
      lookahead = case_labels(p);
    } /* end if */
  } /* end while */
  
  return lookahead;
} /* end case_label_list */


/* --------------------------------------------------------------------------
 * private procedure case_labels()
 * --------------------------------------------------------------------------
 * caseLabels :=
 *   constExpression ( '..' constExpression )?
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t case_labels (m2c_parser_context_t p) {
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("caseLabels");
  
  /* constExpression */
  lookahead = const_expression(p);
  
  /* ( '..' constExpression )? */
  if (lookahead == TOKEN_RANGE) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* constExpression */
    if (match_set(p, FIRST(EXPRESSION), FOLLOW(CASE_LABELS))) {
      lookahead = const_expression(p);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end case_labels */


/* --------------------------------------------------------------------------
 * private procedure pointer_type()
 * --------------------------------------------------------------------------
 * pointerType :=
 *   POINTER TO type
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t pointer_type (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("pointerType");
  
  /* POINTER */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* TO */
  if (match_token(p, TOKEN_TO, FOLLOW(POINTER_TYPE))) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* type */
    if (match_set(p, FIRST(TYPE), FOLLOW(POINTER_TYPE))) {
      lookahead = countable_type(p);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end pointer_type */


/* --------------------------------------------------------------------------
 * private procedure procedure_type()
 * --------------------------------------------------------------------------
 * procedureType :=
 *   PROCEDURE ( '(' ( formalType ( ',' formalType )* )? ')' )?
 *   ( ':' returnedType )?
 *   ;
 *
 * returnedType := typeIdent ;
 * ----------------------------------------------------------------------- */

m2c_token_t formal_type (m2c_parser_context_t p);

m2c_token_t procedure_type (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("procedureType");
  
  /* PROCEDURE */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* ( '(' ( formalType ( ',' formalType )* )? ')' )? */
  if (lookahead == TOKEN_LEFT_PAREN) {
    /* '(' */
    lookahead = m2c_consume_sym(p->lexer);
    
    /* formalType */
    if (lookahead != TOKEN_RIGHT_PAREN) {
      if (match_set(p, FIRST(FORMAL_TYPE), RESYNC(COMMA_OR_RIGHT_PAREN))) {
        lookahead = formal_type(p);
      }
      else /* resync */ {
        lookahead = m2c_next_sym(p->lexer);
      } /* end if */
      
      /* ( ',' formalType )* */
      while (lookahead == TOKEN_COMMA) {
        /* ',' */
        lookahead = m2c_consume_sym(p->lexer);
      
        /* formalType */
        if (match_set(p, FIRST(FORMAL_TYPE), RESYNC(COMMA_OR_RIGHT_PAREN))) {
          lookahead = formal_type(p);
        }
        else /* resync */ {
          lookahead = m2c_next_sym(p->lexer);
        } /* end if */
      } /* end while */
    } /* end if */
    
    /* ')' */
    if (match_token(p, TOKEN_RIGHT_PAREN, RESYNC(COLON_OR_SEMICOLON))) {
      lookahead = m2c_consume_sym(p->lexer);
    }
    else /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  /* ( ':' returnedType )? */
  if (lookahead == TOKEN_COLON) {
    /* ':' */
    lookahead = m2c_consume_sym(p->lexer);
    
    /* returnedType */
    if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(PROCEDURE_TYPE))) {
      lookahead = qualident(p);
    }
    else /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end procedure_type */


/* --------------------------------------------------------------------------
 * private procedure formal_type()
 * --------------------------------------------------------------------------
 * formalType :=
 *   simpleFormalType | attributedFormalType
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t simple_formal_type (m2c_parser_context_t p);

m2c_token_t attributed_formal_type (m2c_parser_context_t p);

m2c_token_t formal_type (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("formalType");
  
  lookahead = m2c_next_sym(p->lexer);
  
  /* simpleFormalType */
  if ((lookahead == TOKEN_ARRAY) || (lookahead == TOKEN_IDENTIFIER)) {
    lookahead = simple_formal_type(p);
  }
  /* | attributedFormalType */
  else if ((lookahead == TOKEN_CONST) || (lookahead == TOKEN_VAR)) {
    lookahead = attributed_formal_type(p);
  }
  else /* unreachable code */ {
    /* fatal error -- abort */
    exit(-1);
  } /* end if */
  
  return lookahead;
} /* end formal_type */


/* --------------------------------------------------------------------------
 * private procedure simple_formal_type()
 * --------------------------------------------------------------------------
 * simpleFormalType :=
 *   ( ARRAY OF )? typeIdent
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t simple_formal_type (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("simpleFormalType");
  
  lookahead = m2c_next_sym(p->lexer);
  
  /* ( ARRAY OF )? */
  if (lookahead == TOKEN_ARRAY) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* OF */
    if (match_token(p, TOKEN_OF, FOLLOW(SIMPLE_FORMAL_TYPE))) {
      lookahead = m2c_consume_sym(p->lexer);
    }
    else /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  /* typeIdent */
  if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(SIMPLE_FORMAL_TYPE))) {
    lookahead = qualident(p);
  } /* end if */
  
  return lookahead;
} /* end simple_formal_type */


/* --------------------------------------------------------------------------
 * private procedure attributed_formal_type()
 * --------------------------------------------------------------------------
 * attributedFormalType :=
 *   ( CONST | VAR ) simpleFormalType
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t attributed_formal_type (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("attributedFormalType");
  
  lookahead = m2c_next_sym(p->lexer);
  
  /* CONST */
  if (lookahead == TOKEN_CONST) {
    lookahead = m2c_consume_sym(p->lexer);
  }
  /* | VAR */
  else if (lookahead == TOKEN_VAR) {
    lookahead = m2c_consume_sym(p->lexer);
  }
  else /* unreachable code */ {
    /* fatal error -- abort */
    exit(-1);
  } /* end if */
  
  /* simpleFormalType */
  if (match_set(p, FIRST(SIMPLE_FORMAL_TYPE),
      FOLLOW(ATTRIBUTED_FORMAL_TYPE))) {
    lookahead = simple_formal_type(p);
  } /* end if */

  return lookahead;
} /* end attributed_formal_type */


/* --------------------------------------------------------------------------
 * private procedure procedure_header()
 * --------------------------------------------------------------------------
 * procedureHeader :=
 *   PROCEDURE procedureSignature
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t procedure_signature (m2c_parser_context_t p);

m2c_token_t procedure_header (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("procedureHeader");
  
  /* PROCEDURE */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* procedureSignature */
  if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(PROCEDURE_HEADER))) {
    lookahead = procedure_signature(p);
  } /* end if */

  return lookahead;
} /* end procedure_header */


/* --------------------------------------------------------------------------
 * private procedure procedure_signature()
 * --------------------------------------------------------------------------
 * procedureSignature :=
 *   Ident ( '(' formalParamList? ')' ( ':' returnedType )? )?
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t formal_param_list (m2c_parser_context_t p);

m2c_token_t procedure_signature (m2c_parser_context_t p) {
  m2c_string_t ident;
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("procedureSignature");
  
  /* Ident */
  lookahead = m2c_consume_sym(p->lexer);
  ident = m2c_lexer_current_lexeme(p->lexer);
  
  /* ( '(' formalParamList? ')' ( ':' returnedType )? )? */
  if (lookahead == TOKEN_LEFT_PAREN) {
    
    /* '(' */
    lookahead = m2c_consume_sym(p->lexer);
    
    /* formalParamList? */
    if ((lookahead == TOKEN_IDENTIFIER) ||
        (lookahead == TOKEN_VAR)) {
      lookahead = formal_param_list(p);
    } /* end if */
    
    /* ')' */
    if (match_token(p, TOKEN_RIGHT_PAREN, FOLLOW(PROCEDURE_TYPE))) {
      lookahead = m2c_consume_sym(p->lexer);
    }
    else /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
    
    /* ( ':' returnedType )? */
    if (lookahead == TOKEN_COLON) {
      /* ':' */
      lookahead = m2c_consume_sym(p->lexer);
    
      /* returnedType */
      if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(PROCEDURE_TYPE))) {
        lookahead = qualident(p);
      } /* end if */
    } /* end if */
  } /* end if */
    
  return lookahead;
} /* end procedure_signature */


/* --------------------------------------------------------------------------
 * private procedure formal_param_list()
 * --------------------------------------------------------------------------
 * formalParamList :=
 *   formalParams ( ';' formalParams )*
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t formal_params (m2c_parser_context_t p);

m2c_token_t formal_param_list (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  uint_t line_of_semicolon, column_of_semicolon;
  
  PARSER_DEBUG_INFO("formalParamList");
  
  /* formalParams */
  lookahead = formal_params(p);
  
  /* ( ';' formalParams )* */
  while (lookahead == TOKEN_SEMICOLON) {
    /* ';' */
    line_of_semicolon = m2c_lexer_lookahead_line(p->lexer);
    column_of_semicolon = m2c_lexer_lookahead_column(p->lexer);
    lookahead = m2c_consume_sym(p->lexer);
    
    /* check if semicolon occurred at the end of a formal parameter list */
    if (lookahead == TOKEN_RIGHT_PAREN) {
    
      if (m2c_option_errant_semicolon()) {
        /* treat as warning */
        m2c_emit_warning_w_pos
          (M2C_SEMICOLON_AFTER_FORMAL_PARAM_LIST,
           line_of_semicolon, column_of_semicolon);
      }
      else /* treat as error */ {
        m2c_emit_error_w_pos
          (M2C_SEMICOLON_AFTER_FORMAL_PARAM_LIST,
           line_of_semicolon, column_of_semicolon);
        p->error_count++;
      } /* end if */
      
      /* print source line */
      if (m2c_option_verbose()) {
        m2c_print_line_and_mark_column(p->lexer,
          line_of_semicolon, column_of_semicolon);
      } /* end if */
    
      /* leave field list sequence loop to continue */
      break;
    } /* end if */
    
    /* formalParams */
    if (match_set(p, FIRST(FORMAL_PARAMS), FOLLOW(FORMAL_PARAMS))) {
      lookahead = formal_params(p);
    } /* end if */
  } /* end while */
  
  return lookahead;
} /* end formal_param_list */


/* --------------------------------------------------------------------------
 * private procedure formal_params()
 * --------------------------------------------------------------------------
 * formalParams :=
 *   simpleFormalParams | attribFormalParams
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t simple_formal_params (m2c_parser_context_t p);

m2c_token_t attrib_formal_params (m2c_parser_context_t p);

m2c_token_t formal_params (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("formalParams");
  
  lookahead = m2c_next_sym(p->lexer);
  
  /* simpleFormalParams */
  if (lookahead == TOKEN_IDENTIFIER) {
    lookahead = simple_formal_params(p);
  }
  /* | attribFormalParams */
  else if ((lookahead == TOKEN_CONST) || (lookahead == TOKEN_VAR)) {
    lookahead = attrib_formal_params(p);
  }
  else /* unreachable code */ {
    /* fatal error -- abort */
      exit(-1);
  } /* end if */
  
  return lookahead;
} /* end formal_params */


/* --------------------------------------------------------------------------
 * private procedure simple_formal_params()
 * --------------------------------------------------------------------------
 * simpleFormalParams :=
 *   identList ':' formalType
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t simple_formal_params (m2c_parser_context_t p) {
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("simpleFormalParams");
  
  /* IdentList */
  lookahead = ident_list(p);
  
  /* ':' */
  if (match_token(p, TOKEN_COLON, FOLLOW(SIMPLE_FORMAL_PARAMS))) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* formalType */
    if (match_set(p, FIRST(FORMAL_TYPE), FOLLOW(SIMPLE_FORMAL_PARAMS))) {
      lookahead = formal_type(p);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end simple_formal_params */


/* --------------------------------------------------------------------------
 * private procedure attrib_formal_params()
 * --------------------------------------------------------------------------
 * attribFormalParams :=
 *   ( CONST | VAR ) simpleFormalParams
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t attrib_formal_params (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("attribFormalParams");
  
  lookahead = m2c_next_sym(p->lexer);
  
  /* CONST */
  if (lookahead == TOKEN_CONST) {
    lookahead = m2c_consume_sym(p->lexer);
  }
  /* | VAR */
  else if (lookahead == TOKEN_VAR) {
    lookahead = m2c_consume_sym(p->lexer);
  }
  else /* unreachable code */ {
    /* fatal error -- abort */
    exit(-1);
  } /* end if */
  
  /* simpleFormalParams */
  if (match_set(p, FIRST(SIMPLE_FORMAL_PARAMS),
      FOLLOW(ATTRIB_FORMAL_PARAMS))) {
    lookahead = simple_formal_params(p);
  } /* end if */

  return lookahead;
} /* end attrib_formal_params */


/* ************************************************************************ *
 * Implementation and Program Module Syntax                                 *
 * ************************************************************************ */


/* --------------------------------------------------------------------------
 * private procedure implementation_module()
 * --------------------------------------------------------------------------
 * implementationModule :=
 *   IMPLEMENTATION programModule
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t implementation_module (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("implementationModule");
  
  /* IMPLEMENTATION */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* programModule */
  if (match_token(p, TOKEN_MODULE, FOLLOW(PROGRAM_MODULE))) {
    lookahead = program_module(p);
  } /* end if */

  return lookahead;
} /* end implementation_module */


/* --------------------------------------------------------------------------
 * private procedure program_module()
 * --------------------------------------------------------------------------
 * programModule :=
 *   MODULE moduleIdent modulePriority? ';'
 *   import* block moduleIdent '.'
 *   ;
 *
 * moduleIdent := Ident ;
 * ----------------------------------------------------------------------- */

m2c_token_t module_priority (m2c_parser_context_t p);

m2c_token_t block (m2c_parser_context_t p);

m2c_token_t program_module (m2c_parser_context_t p) {
  m2c_string_t module_ident_header, module_ident_end;
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("programModule");
  
  /* MODULE */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* moduleIdent */
  if (match_token(p, TOKEN_IDENTIFIER, RESYNC(IMPORT_OR_BLOCK))) {
    lookahead = m2c_consume_sym(p->lexer);
    module_ident_header = m2c_lexer_current_lexeme(p->lexer);
    
    /* modulePriority? */
    if (lookahead == TOKEN_LEFT_BRACKET) {
      lookahead = module_priority(p);
    } /* end while */
    
    /* ';' */
    if (match_token(p, TOKEN_SEMICOLON, RESYNC(IMPORT_OR_BLOCK))) {
      lookahead = m2c_consume_sym(p->lexer);
    }
    else /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
  }
  else /* resync */ {
    lookahead = m2c_next_sym(p->lexer);
  } /* end if */
  
  /* import* */
  while ((lookahead == TOKEN_IMPORT) ||
         (lookahead == TOKEN_FROM)) {
    lookahead = import(p);
  } /* end while */
  
  /* block */
  if (match_set(p, FIRST(BLOCK), FOLLOW(PROGRAM_MODULE))) {
    lookahead = block(p);
    
    /* moduleIdent */
    if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(PROGRAM_MODULE))) {
      lookahead = m2c_consume_sym(p->lexer);
      module_ident_end = m2c_lexer_current_lexeme(p->lexer);
      
      if (module_ident_header != module_ident_end) {
        /* error: module identifiers don't match */ 
      } /* end if */
      
      if (match_token(p, TOKEN_PERIOD, FOLLOW(PROGRAM_MODULE))) {
        lookahead = m2c_consume_sym(p->lexer);
      } /* end if */
    } /* end if */
  } /* end if */  
  
  return lookahead;
} /* end program_module */


/* --------------------------------------------------------------------------
 * private procedure module_priority()
 * --------------------------------------------------------------------------
 * modulePriority :=
 *   '[' constExpression ']'
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t module_priority (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("modulePriority");
  
  /* '[' */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* constExpression */
  if (match_set(p, FIRST(EXPRESSION), FOLLOW(MODULE_PRIORITY))) {
    lookahead = const_expression(p);
    
    /* ']' */
    if (match_token(p, TOKEN_RIGHT_BRACKET, FOLLOW(MODULE_PRIORITY))) {
      lookahead = m2c_consume_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end module_priority */


/* --------------------------------------------------------------------------
 * private procedure block()
 * --------------------------------------------------------------------------
 * block :=
 *   declaration* ( BEGIN statementSequence )? END
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t declaration (m2c_parser_context_t p);

m2c_token_t statement_sequence (m2c_parser_context_t p);

m2c_token_t block (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("block");
  
  lookahead = m2c_next_sym(p->lexer);
  
  /* declaration* */
  while ((lookahead == TOKEN_CONST) ||
         (lookahead == TOKEN_TYPE) ||
         (lookahead == TOKEN_VAR) ||
         (lookahead == TOKEN_PROCEDURE) ||
         (lookahead == TOKEN_MODULE)) {
    lookahead = declaration(p);
  } /* end while */
  
  /* ( BEGIN statementSequence )? */
  if (lookahead == TOKEN_BEGIN) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* check for empty statement sequence */
    if ((m2c_tokenset_element(FOLLOW(STATEMENT_SEQUENCE), lookahead))) {
    
        /* print warning */
        m2c_emit_warning_w_pos
          (M2C_EMPTY_STMT_SEQ,
           m2c_lexer_lookahead_line(p->lexer),
           m2c_lexer_lookahead_column(p->lexer));
    }
    
    /* statementSequence */
    else if (match_set(p, FIRST(STATEMENT_SEQUENCE), FOLLOW(STATEMENT))) {
      lookahead = statement_sequence(p);
    }
    else /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  /* END */
  if (match_token(p, TOKEN_END, FOLLOW(BLOCK))) {
    lookahead = m2c_consume_sym(p->lexer);
  } /* end if */
  
  return lookahead;
} /* end block */


/* --------------------------------------------------------------------------
 * private procedure declaration()
 * --------------------------------------------------------------------------
 * declaration :=
 *   CONST ( constDeclaration ';' )* |
 *   TYPE ( typeDeclaration ';' )* |
 *   VAR ( variableDeclaration ';' )* |
 *   procedureDeclaration ';'
 *   moduleDeclaration ';'
 *   ;
 * 
 * constDeclaration := constDefinition ;
 * ----------------------------------------------------------------------- */

m2c_token_t type_declaration (m2c_parser_context_t p);

m2c_token_t variable_declaration (m2c_parser_context_t p);

m2c_token_t procedure_declaration (m2c_parser_context_t p);

m2c_token_t module_declaration (m2c_parser_context_t p);

m2c_token_t declaration (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("declaration");
  
  lookahead = m2c_next_sym(p->lexer);
  
  switch (lookahead) {
    
    /* CONST */
    case TOKEN_CONST :
      lookahead = m2c_consume_sym(p->lexer);
      
      /* ( constDeclaration ';' )* */
      while (lookahead == TOKEN_IDENTIFIER) {
        lookahead = const_definition(p);
        
        /* ';' */
        if (match_token(p, TOKEN_SEMICOLON,
            RESYNC(DECLARATION_OR_IDENT_OR_SEMICOLON))) {
          lookahead = m2c_consume_sym(p->lexer);
        } /* end if */
      } /* end while */
      break;
      
    /* | TYPE */
    case TOKEN_TYPE :
      lookahead = m2c_consume_sym(p->lexer);
      
      /* ( typeDeclaration ';' )* */
      while (lookahead == TOKEN_IDENTIFIER) {
        lookahead = type_declaration(p);
        
        /* ';' */
        if (match_token(p, TOKEN_SEMICOLON,
            RESYNC(DECLARATION_OR_IDENT_OR_SEMICOLON))) {
          lookahead = m2c_consume_sym(p->lexer);
        } /* end if */
      } /* end while */
      break;
      
    /* | VAR */
    case TOKEN_VAR :
      lookahead = m2c_consume_sym(p->lexer);
      
      /* ( variableDeclaration ';' )* */
      while (lookahead == TOKEN_IDENTIFIER) {
        lookahead = variable_declaration(p);
        
        /* ';' */
        if (match_token(p, TOKEN_SEMICOLON,
            RESYNC(DECLARATION_OR_IDENT_OR_SEMICOLON))) {
          lookahead = m2c_consume_sym(p->lexer);
        } /* end if */
      } /* end while */
      break;
      
    /* | procedureDeclaration ';' */
    case TOKEN_PROCEDURE :
      lookahead = procedure_declaration(p);
      
      /* ';' */
      if (match_token(p, TOKEN_SEMICOLON,
          RESYNC(DECLARATION_OR_SEMICOLON))) {
        lookahead = m2c_consume_sym(p->lexer);
      } /* end if */
      break;
      
    /* | moduleDeclaration ';' */
    case TOKEN_MODULE :
      lookahead = module_declaration(p);
      
      /* ';' */
      if (match_token(p, TOKEN_SEMICOLON,
          RESYNC(DECLARATION_OR_SEMICOLON))) {
        lookahead = m2c_consume_sym(p->lexer);
      } /* end if */
      break;
      
    default : /* unreachable code */
      /* fatal error -- abort */
      exit(-1);
  } /* end switch */
  
  return lookahead;
} /* end declaration */


/* --------------------------------------------------------------------------
 * private procedure type_declaration()
 * --------------------------------------------------------------------------
 * typeDeclaration :=
 *   Ident '=' ( type | varSizeRecordType )
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t var_size_record_type (m2c_parser_context_t p);

m2c_token_t type_declaration (m2c_parser_context_t p) {
  m2c_string_t ident;
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("typeDeclaration");
  
  /* Ident */
  lookahead = m2c_consume_sym(p->lexer);
  ident = m2c_lexer_current_lexeme(p->lexer);
  
  /* '=' */
  if (match_token(p, TOKEN_EQUAL, FOLLOW(TYPE_DECLARATION))) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* type | varSizeRecordType */
    if (match_set(p, FIRST(TYPE_DECLARATION_TAIL),
        FOLLOW(TYPE_DECLARATION))) {
      
      /* type */
      if (lookahead != TOKEN_VAR) {
        lookahead = type(p);
      }
      
      /* | varSizeRecordType */
      else {
        lookahead = var_size_record_type(p);
      } /* end if */
    
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end type_declaration */


/* --------------------------------------------------------------------------
 * private procedure var_size_record_type()
 * --------------------------------------------------------------------------
 * varSizeRecordType :=
 *   VAR RECORD fieldListSequence
 *   VAR varSizeFieldIdent ':' ARRAY sizeFieldIdent OF typeIdent
 *   END
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t var_size_record_type (m2c_parser_context_t p) {
  m2c_string_t var_size_field, size_field;
  uint_t line_of_semicolon, column_of_semicolon;
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("varSizeRecordType");
  
  /* VAR */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* RECORD */
  if (match_token(p, TOKEN_RECORD, FOLLOW(VAR_SIZE_RECORD_TYPE))) {
    lookahead = m2c_consume_sym(p->lexer); 
    
    /* check for empty field list sequence */
    if (lookahead == TOKEN_VAR) {

        /* empty field list sequence warning */
        m2c_emit_warning_w_pos
          (M2C_EMPTY_FIELD_LIST_SEQ,
           m2c_lexer_lookahead_line(p->lexer),
           m2c_lexer_lookahead_column(p->lexer));
    }
  
    /* fieldListSequence */
    else if (match_set(p, FIRST(FIELD_LIST_SEQUENCE),
             FOLLOW(VAR_SIZE_RECORD_TYPE))) {
      lookahead = field_list_sequence(p);
    }
    else /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
    
    /* VAR */
    if (match_token(p, TOKEN_VAR, FOLLOW(VAR_SIZE_RECORD_TYPE))) {
      lookahead = m2c_consume_sym(p->lexer);
      
      if (lookahead == TOKEN_END) {
        m2c_emit_warning_w_pos
          (M2C_EMPTY_FIELD_LIST_SEQ,
           m2c_lexer_lookahead_line(p->lexer),
           m2c_lexer_lookahead_column(p->lexer));
           m2c_consume_sym(p->lexer);
      }
      /* varSizeFieldIdent */
      else if (match_token(p, TOKEN_IDENTIFIER,
               FOLLOW(VAR_SIZE_RECORD_TYPE))) {
        lookahead = m2c_consume_sym(p->lexer);
        var_size_field = m2c_lexer_current_lexeme(p->lexer);
      
        /* ':' */
        if (match_token(p, TOKEN_COLON, FOLLOW(VAR_SIZE_RECORD_TYPE))) {
          lookahead = m2c_consume_sym(p->lexer);
        
          /* ARRAY */
          if (match_token(p, TOKEN_ARRAY, FOLLOW(VAR_SIZE_RECORD_TYPE))) {
            lookahead = m2c_consume_sym(p->lexer);
          
            /* sizeFieldIdent */
            if (match_token(p, TOKEN_IDENTIFIER,
                FOLLOW(VAR_SIZE_RECORD_TYPE))) {
              lookahead = m2c_consume_sym(p->lexer);
              size_field = m2c_lexer_current_lexeme(p->lexer);
            
              /* OF */
              if (match_token(p, TOKEN_OF, FOLLOW(VAR_SIZE_RECORD_TYPE))) {
                lookahead = m2c_consume_sym(p->lexer);
              
                /* typeIdent */
                if (match_token(p, TOKEN_IDENTIFIER,
                    FOLLOW(VAR_SIZE_RECORD_TYPE))) {
                  lookahead = m2c_consume_sym(p->lexer);
                  
                  /* check for errant semicolon */
                  if (lookahead == TOKEN_SEMICOLON) {
                    line_of_semicolon =
                      m2c_lexer_lookahead_line(p->lexer);
                    column_of_semicolon =
                      m2c_lexer_lookahead_column(p->lexer);
                  
                    if (m2c_option_errant_semicolon()) {
                      /* treat as warning */
                      m2c_emit_warning_w_pos
                        (M2C_SEMICOLON_AFTER_FIELD_LIST_SEQ,
                         line_of_semicolon, column_of_semicolon);
                    }
                    else /* treat as error */ {
                      m2c_emit_error_w_pos
                        (M2C_SEMICOLON_AFTER_FIELD_LIST_SEQ,
                         line_of_semicolon, column_of_semicolon);
                      p->error_count++;
                    } /* end if */
                    
                    m2c_consume_sym(p->lexer);
                    
                    /* print source line */
                    if (m2c_option_verbose()) {
                      m2c_print_line_and_mark_column(p->lexer,
                        line_of_semicolon, column_of_semicolon);
                    } /* end if */
                  } /* end if */
                  
                  if (match_token(p, TOKEN_END,
                      FOLLOW(VAR_SIZE_RECORD_TYPE))) {
                    lookahead = m2c_consume_sym(p->lexer);
                  } /* end if */
                } /* end if */
              } /* end if */
            } /* end if */
          } /* end if */
        } /* end if */
      } /* end if */
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end var_size_record_type */


/* --------------------------------------------------------------------------
 * private procedure variable_declaration()
 * --------------------------------------------------------------------------
 * variableDeclaration :=
 *   identList ':' type
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t variable_declaration (m2c_parser_context_t p) {
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("variableDeclaration");
  
  /* IdentList */
  lookahead = ident_list(p);
  
  /* ':' */
  if (match_token(p, TOKEN_COLON, FOLLOW(VARIABLE_DECLARATION))) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* type */
    if (match_set(p, FIRST(TYPE), FOLLOW(VARIABLE_DECLARATION))) {
      lookahead = type(p);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end variable_declaration */


/* --------------------------------------------------------------------------
 * private procedure procedure_declaration()
 * --------------------------------------------------------------------------
 * procedureDeclaration :=
 *   procedureHeader ';' block Ident
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t procedure_declaration (m2c_parser_context_t p) {
  m2c_string_t ident;
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("procedureDeclaration");
  
  /* procedureHeader */
  lookahead = procedure_header(p);
  
  /* ';' */
  if (match_token(p, TOKEN_SEMICOLON, FOLLOW(PROCEDURE_DECLARATION))) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* block */
    if (match_set(p, FIRST(BLOCK), FOLLOW(PROCEDURE_DECLARATION))) {
      lookahead = block(p);
      
      /* Ident */
      if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(PROCEDURE_DECLARATION))) {
        lookahead = m2c_consume_sym(p->lexer);
        ident = m2c_lexer_current_lexeme(p->lexer);
            
      } /* end if */
    } /* end if */
  } /* end if */
      
  return lookahead;
} /* end procedure_declaration */


/* --------------------------------------------------------------------------
 * private procedure module_declaration()
 * --------------------------------------------------------------------------
 * moduleDeclaration :=
 *   MODULE moduleIdent modulePriority? ';'
 *   import* export? block moduleIdent
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t export (m2c_parser_context_t p);

m2c_token_t module_declaration (m2c_parser_context_t p) {
  m2c_string_t module_ident_header, module_ident_end;
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("moduleDeclaration");
  
  /* MODULE */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* moduleIdent */
  if (match_token(p, TOKEN_IDENTIFIER, RESYNC(IMPORT_OR_BLOCK))) {
    lookahead = m2c_consume_sym(p->lexer);
    module_ident_header = m2c_lexer_current_lexeme(p->lexer);
    
    /* modulePriority? */
    if (lookahead == TOKEN_LEFT_BRACKET) {
      lookahead = module_priority(p);
    } /* end while */
    
    /* ';' */
    if (match_token(p, TOKEN_SEMICOLON, RESYNC(IMPORT_OR_BLOCK))) {
      lookahead = m2c_consume_sym(p->lexer);
    }
    else /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
  }
  else /* resync */ {
    lookahead = m2c_next_sym(p->lexer);
  } /* end if */
  
  /* import* */
  while ((lookahead == TOKEN_IMPORT) ||
         (lookahead == TOKEN_FROM)) {
    lookahead = import(p);
  } /* end while */
  
  /* export? */
  if (lookahead == TOKEN_EXPORT) {
    lookahead = export(p);
  } /* end while */
  
  /* block */
  if (match_set(p, FIRST(BLOCK), FOLLOW(MODULE_DECLARATION))) {
    lookahead = block(p);
    
    /* moduleIdent */
    if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(MODULE_DECLARATION))) {
      lookahead = m2c_consume_sym(p->lexer);
      module_ident_end = m2c_lexer_current_lexeme(p->lexer);
      
      if (module_ident_header != module_ident_end) {
        /* error: module identifiers don't match */ 
      } /* end if */
    } /* end if */
  } /* end if */  
  
  return lookahead;
} /* end module_declaration */


/* --------------------------------------------------------------------------
 * private procedure export()
 * --------------------------------------------------------------------------
 * export :=
 *   EXPORT QUALIFIED? identList ';'
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t export (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("export");
  
  /* EXPORT */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* QUALIFIED? */
  if (lookahead == TOKEN_QUALIFIED) {
    lookahead = m2c_consume_sym(p->lexer);
  } /* end if */
  
  /* identList */
  if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(EXPORT))) {
    lookahead = ident_list(p);
    
    /* ';' */
    if (match_token(p, TOKEN_SEMICOLON, FOLLOW(EXPORT))) {
      lookahead = m2c_consume_sym(p->lexer);
    } /* end if */
  } /* end if */

  return lookahead;
} /* end export */


/* --------------------------------------------------------------------------
 * private procedure statement_sequence()
 * --------------------------------------------------------------------------
 * statementSequence :=
 *   statement ( ';' statement )*
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t statement (m2c_parser_context_t p);

m2c_token_t statement_sequence (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  uint_t line_of_semicolon, column_of_semicolon;
  
  PARSER_DEBUG_INFO("statementSequence");
  
  /* statement */
  lookahead = statement(p);
  
  /* ( ';' statement )* */
  while (lookahead == TOKEN_SEMICOLON) {
    /* ';' */
    line_of_semicolon = m2c_lexer_lookahead_line(p->lexer);
    column_of_semicolon = m2c_lexer_lookahead_column(p->lexer);
    lookahead = m2c_consume_sym(p->lexer);
    
    /* check if semicolon occurred at the end of a statement sequence */
    if (m2c_tokenset_element(FOLLOW(STATEMENT_SEQUENCE), lookahead)) {
    
      if (m2c_option_errant_semicolon()) {
        /* treat as warning */
        m2c_emit_warning_w_pos
          (M2C_SEMICOLON_AFTER_STMT_SEQ,
           line_of_semicolon, column_of_semicolon);
      }
      else /* treat as error */ {
        m2c_emit_error_w_pos
          (M2C_SEMICOLON_AFTER_STMT_SEQ,
           line_of_semicolon, column_of_semicolon);
        p->error_count++;
      } /* end if */
      
      /* print source line */
      if (m2c_option_verbose()) {
        m2c_print_line_and_mark_column(p->lexer,
          line_of_semicolon, column_of_semicolon);
      } /* end if */
    
      /* leave statement sequence loop to continue */
      break;
    } /* end if */
    
    /* statement */
    if (match_set(p, FIRST(STATEMENT),
        RESYNC(FIRST_OR_FOLLOW_OF_STATEMENT))) {
      lookahead = statement(p);
    } /* end if */
  } /* end while */
  
  return lookahead;
} /* end statement_sequence */


/* --------------------------------------------------------------------------
 * private procedure statement()
 * --------------------------------------------------------------------------
 * statement :=
 *   assignmentOrProcCall | returnStatement | withStatement | ifStatement |
 *   caseStatement | loopStatement | whileStatement | repeatStatement |
 *   forStatement | EXIT
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t assignment_or_proc_call (m2c_parser_context_t p);

m2c_token_t return_statement (m2c_parser_context_t p);

m2c_token_t with_statement (m2c_parser_context_t p);

m2c_token_t if_statement (m2c_parser_context_t p);

m2c_token_t case_statement (m2c_parser_context_t p);

m2c_token_t loop_statement (m2c_parser_context_t p);

m2c_token_t while_statement (m2c_parser_context_t p);

m2c_token_t repeat_statement (m2c_parser_context_t p);

m2c_token_t for_statement (m2c_parser_context_t p);

m2c_token_t statement (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("statement");
  
  lookahead = m2c_next_sym(p->lexer);
  
  switch (lookahead) {
  
    /* assignmentOrProcCall */
    case TOKEN_IDENTIFIER :
      lookahead = assignment_or_proc_call(p);
      break;
      
    /* | returnStatement */
    case TOKEN_RETURN :
      lookahead = return_statement(p);
      break;
      
    /* | withStatement */
    case TOKEN_WITH :
      lookahead = with_statement(p);
      break;
      
    /* | ifStatement */
    case TOKEN_IF :
      lookahead = if_statement(p);
      break;
      
    /* | caseStatement */
    case TOKEN_CASE :
      lookahead = case_statement(p);
      break;
      
    /* | loopStatement */
    case TOKEN_LOOP :
      lookahead = loop_statement(p);
      break;
      
    /* | whileStatement */
    case TOKEN_WHILE :
      lookahead = while_statement(p);
      break;
      
    /* | repeatStatement */
    case TOKEN_REPEAT :
      lookahead = repeat_statement(p);
      break;
      
    /* | forStatement */
    case TOKEN_FOR :
      lookahead = for_statement(p);
      break;
      
    /* | EXIT */
    case TOKEN_EXIT :
      lookahead = procedure_type(p);
      break;
      
    default : /* unreachable code */
      /* fatal error -- abort */
      exit(-1);
    } /* end switch */
  
  return lookahead;
} /* end statement */


/* --------------------------------------------------------------------------
 * private procedure assignment_or_proc_call()
 * --------------------------------------------------------------------------
 * assignmentOrProcCall :=
 *   designator ( ':=' expression | actualParameters )?
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t designator (m2c_parser_context_t p);

m2c_token_t expression (m2c_parser_context_t p);

m2c_token_t actual_parameters (m2c_parser_context_t p);

m2c_token_t assignment_or_proc_call (m2c_parser_context_t p) {
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("assignmentOrProcCall");
  
  /* designator */
  lookahead = designator(p);
  
  /* ( ':=' expression | actualParameters )? */
  if (lookahead == TOKEN_ASSIGN) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* expression */
    if (match_set(p, FIRST(EXPRESSION), FOLLOW(ASSIGNMENT_OR_PROC_CALL))) {
      lookahead = expression(p);
    } /* end if */
  }
  /* | actualParameters */
  else if (lookahead == TOKEN_LEFT_PAREN) {
    lookahead = actual_parameters(p);
  } /* end if */
  
  return lookahead;
} /* end assignment_or_proc_call */


/* --------------------------------------------------------------------------
 * private procedure actual_parameters()
 * --------------------------------------------------------------------------
 * actualParameters :=
 *   '(' expressionList? ')'
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t expression_list (m2c_parser_context_t p);

m2c_token_t actual_parameters (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("actualParameters");
  
  /* '(' */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* expressionList? */
  if (m2c_tokenset_element(FIRST(EXPRESSION_LIST), lookahead)) {
    lookahead = expression_list(p);
  } /* end if */
  
  /* ')' */
  if (match_token(p, TOKEN_RIGHT_PAREN, FOLLOW(ACTUAL_PARAMETERS))) {
    lookahead = m2c_consume_sym(p->lexer);
  } /* end if */
  
  return lookahead;
} /* end actual_parameters */


/* --------------------------------------------------------------------------
 * private procedure expression_list()
 * --------------------------------------------------------------------------
 * expressionList :=
 *   expression ( ',' expression )*
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t expression_list (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("expressionList");
  
  /* expression */
  lookahead = expression(p);
  
  /* ( ',' expression )* */
  while (lookahead == TOKEN_COMMA) {
    /* ',' */
    lookahead = m2c_consume_sym(p->lexer);
    
    /* expression */
    if (match_set(p, FIRST(EXPRESSION), FOLLOW(EXPRESSION))) {
      lookahead = expression(p);
    } /* end if */
  } /* end while */
  
  return lookahead;
} /* end expression_list */


/* --------------------------------------------------------------------------
 * private procedure return_statement()
 * --------------------------------------------------------------------------
 * returnStatement :=
 *   RETURN expression?
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t return_statement (m2c_parser_context_t p) {
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("returnStatement");
  
  /* RETURN */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* expression? */
  if (m2c_tokenset_element(FIRST(EXPRESSION), lookahead)) {
    lookahead = expression(p);
  } /* end if */
  
  return lookahead;
} /* end return_statement */


/* --------------------------------------------------------------------------
 * private procedure with_statement()
 * --------------------------------------------------------------------------
 * withStatement :=
 *   WITH designator DO statementSequence END
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t with_statement (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("withStatement");
  
  /* WITH */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* designator */
  if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(WITH_STATEMENT))) {
    lookahead = designator(p);
    
    /* DO */
    if (match_token(p, TOKEN_DO, FOLLOW(WITH_STATEMENT))) {
      lookahead = m2c_consume_sym(p->lexer);
      
      /* check for empty statement sequence */
      if (lookahead == TOKEN_END) {
    
        /* empty statement sequence warning */
        m2c_emit_warning_w_pos
          (M2C_EMPTY_STMT_SEQ,
           m2c_lexer_lookahead_line(p->lexer),
           m2c_lexer_lookahead_column(p->lexer));
             
        /* END */
        lookahead = m2c_consume_sym(p->lexer);
      }
    
      /* statementSequence */
      else if (match_set(p, FIRST(STATEMENT_SEQUENCE),
               FOLLOW(WITH_STATEMENT))) {
        lookahead = statement_sequence(p);
        
        /* END */
        if (match_token(p, TOKEN_END, FOLLOW(WITH_STATEMENT))) {
          lookahead = m2c_consume_sym(p->lexer);
        } /* end if */
      } /* end if */
    } /* end if */
  } /* end if */
      
  return lookahead;
} /* end with_statement */


/* --------------------------------------------------------------------------
 * private procedure if_statement()
 * --------------------------------------------------------------------------
 * ifStatement :=
 *   IF boolExpression THEN statementSequence
 *   ( ELSIF boolExpression THEN statementSequence )*
 *   ( ELSE statementSequence )?
 *   END
 *   ;
 *
 * boolExpression := expression ;
 * ----------------------------------------------------------------------- */

m2c_token_t if_statement (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("ifStatement");
  
  /* IF */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* boolExpression */
  if (match_set(p, FIRST(EXPRESSION), RESYNC(ELSIF_OR_ELSE_OR_END))) {
    lookahead = expression(p);
    
    /* THEN */
    if (match_token(p, TOKEN_THEN, RESYNC(ELSIF_OR_ELSE_OR_END))) {
      lookahead = m2c_consume_sym(p->lexer);
      
      /* check for empty statement sequence */
      if ((m2c_tokenset_element(RESYNC(ELSIF_OR_ELSE_OR_END), lookahead))) {
    
          /* empty statement sequence warning */
          m2c_emit_warning_w_pos
            (M2C_EMPTY_STMT_SEQ,
             m2c_lexer_lookahead_line(p->lexer),
             m2c_lexer_lookahead_column(p->lexer));
      }
      
      /* statementSequence */
      else if (match_set(p, FIRST(STATEMENT_SEQUENCE),
          RESYNC(ELSIF_OR_ELSE_OR_END))) {
        lookahead = statement_sequence(p);
      }
      else /* resync */ {
        lookahead = m2c_next_sym(p->lexer);
      } /* end if */
    }
    else /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  /* ( ELSIF boolExpression THEN statementSequence )* */
  while (lookahead == TOKEN_ELSIF) {
    
    /* ELSIF */
    lookahead = m2c_consume_sym(p->lexer);
    
    /* boolExpression */
    if (match_set(p, FIRST(EXPRESSION), RESYNC(ELSIF_OR_ELSE_OR_END))) {
      lookahead = expression(p);
    
      /* THEN */
      if (match_token(p, TOKEN_THEN, RESYNC(ELSIF_OR_ELSE_OR_END))) {
        lookahead = m2c_consume_sym(p->lexer);
      
        /* check for empty statement sequence */
        if ((m2c_tokenset_element
            (RESYNC(ELSIF_OR_ELSE_OR_END), lookahead))) {
    
            /* empty statement sequence warning */
            m2c_emit_warning_w_pos
              (M2C_EMPTY_STMT_SEQ,
               m2c_lexer_lookahead_line(p->lexer),
               m2c_lexer_lookahead_column(p->lexer));
        }
      
        /* statementSequence */
        else if (match_set(p, FIRST(STATEMENT_SEQUENCE),
            RESYNC(ELSIF_OR_ELSE_OR_END))) {
          lookahead = statement_sequence(p);
        }
        else /* resync */ {
          lookahead = m2c_next_sym(p->lexer);
        } /* end if */
      }
      else /* resync */ {
        lookahead = m2c_next_sym(p->lexer);
      } /* end if */
    }
    else /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
  } /* end while */
  
  /* ( ELSE statementSequence )? */
  if (lookahead == TOKEN_ELSE) {
  
    /* ELSE */
    lookahead = m2c_consume_sym(p->lexer);
  
    /* check for empty statement sequence */
    if (lookahead == TOKEN_END) {
  
        /* empty statement sequence warning */
        m2c_emit_warning_w_pos
          (M2C_EMPTY_STMT_SEQ,
           m2c_lexer_lookahead_line(p->lexer),
           m2c_lexer_lookahead_column(p->lexer));
    }
    
    /* statementSequence */
    else if (match_set(p, FIRST(STATEMENT_SEQUENCE), FOLLOW(IF_STATEMENT))) {
      lookahead = statement_sequence(p);
    }
    else /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  /* END */
  if (match_token(p, TOKEN_END, FOLLOW(IF_STATEMENT))) {
    lookahead = m2c_consume_sym(p->lexer);
  } /* end if */
  
  return lookahead;
} /* end if_statement */


/* --------------------------------------------------------------------------
 * private procedure case_statement()
 * --------------------------------------------------------------------------
 * caseStatement :=
 *   CASE expression OF case ( '|' case )*
 *   ( ELSE statementSequence )?
 *   END
 *   ;
 *
 * NB: 'case' is a reserved word in C -> we use case_branch() here instead
 * ----------------------------------------------------------------------- */

m2c_token_t case_branch (m2c_parser_context_t p);

m2c_token_t case_statement (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("caseStatement");
  
  /* CASE */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* expression */
  if (match_set(p, FIRST(EXPRESSION), RESYNC(ELSE_OR_END))) {
    lookahead = expression(p);
    
    /* OF */
    if (match_token(p, TOKEN_OF, RESYNC(ELSE_OR_END))) {
      lookahead = m2c_consume_sym(p->lexer);
      
      /* case */
      if (match_set(p, FIRST(CASE), RESYNC(ELSE_OR_END))) {
        lookahead = case_branch(p);
        
        /* ( '| case )* */
        while (lookahead == TOKEN_BAR) {
          /* '|' */
          lookahead = m2c_consume_sym(p->lexer);
          
          /* case */
          if (match_set(p, FIRST(CASE), RESYNC(ELSE_OR_END))) {
            lookahead = case_branch(p);
          }
          else /* resync */ {
            lookahead = m2c_next_sym(p->lexer);
          } /* end if */
        } /* end while */
      }
      else /* resync */ {
        lookahead = m2c_next_sym(p->lexer);
      } /* end if */
    }
    else /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  /* ( ELSE statementSequence )? */
  if (lookahead == TOKEN_ELSE) {
  
    /* ELSE */
    lookahead = m2c_consume_sym(p->lexer);
  
    /* check for empty statement sequence */
    if (lookahead == TOKEN_END) {
  
        /* empty statement sequence warning */
        m2c_emit_warning_w_pos
          (M2C_EMPTY_STMT_SEQ,
           m2c_lexer_lookahead_line(p->lexer),
           m2c_lexer_lookahead_column(p->lexer));
    }
    
    /* statementSequence */
    else if
      (match_set(p, FIRST(STATEMENT_SEQUENCE), FOLLOW(CASE_STATEMENT))) {
      lookahead = statement_sequence(p);
    }
    else /* resync */ {
      lookahead = m2c_next_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  /* END */
  if (match_token(p, TOKEN_END, FOLLOW(CASE_STATEMENT))) {
    lookahead = m2c_consume_sym(p->lexer);
  } /* end if */
  
  return lookahead;
} /* end case_statement */


/* --------------------------------------------------------------------------
 * private procedure case_branch()
 * --------------------------------------------------------------------------
 * case :=
 *   caseLabelList ':' statementSequence
 *   ;
 *
 * NB: 'case' is a reserved word in C -> we use case_branch() here instead
 * ----------------------------------------------------------------------- */

m2c_token_t case_branch (m2c_parser_context_t p) {
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("case");
  
  /* caseLabelList */
  lookahead = case_label_list(p);
  
  /* ':' */
  if (match_token(p, TOKEN_COLON, FOLLOW(CASE))) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* check for empty statement sequence */
    if ((m2c_tokenset_element(FOLLOW(CASE), lookahead))) {
  
        /* empty statement sequence warning */
        m2c_emit_warning_w_pos
          (M2C_EMPTY_STMT_SEQ,
           m2c_lexer_lookahead_line(p->lexer),
           m2c_lexer_lookahead_column(p->lexer));
    }
    
    /* statementSequence */
    else if (match_set(p, FIRST(STATEMENT_SEQUENCE), FOLLOW(CASE))) {
      lookahead = statement_sequence(p);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end case_branch */


/* --------------------------------------------------------------------------
 * private procedure loop_statement()
 * --------------------------------------------------------------------------
 * loopStatement :=
 *   LOOP statementSequence END
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t loop_statement (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("loopStatement");
  
  /* LOOP */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* check for empty statement sequence */
  if (lookahead == TOKEN_END) {

    /* empty statement sequence warning */
    m2c_emit_warning_w_pos
      (M2C_EMPTY_STMT_SEQ,
       m2c_lexer_lookahead_line(p->lexer),
       m2c_lexer_lookahead_column(p->lexer));
         
    /* END */
    lookahead = m2c_consume_sym(p->lexer);
  }

  /* statementSequence */
  else if (match_set(p, FIRST(STATEMENT_SEQUENCE), FOLLOW(LOOP_STATEMENT))) {
    lookahead = statement_sequence(p);
    
    /* END */
    if (match_token(p, TOKEN_END, FOLLOW(LOOP_STATEMENT))) {
      lookahead = m2c_consume_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end loop_statement */


/* --------------------------------------------------------------------------
 * private procedure while_statement()
 * --------------------------------------------------------------------------
 * whileStatement :=
 *   WHILE boolExpression DO statementSequence END
 *   ;
 *
 * boolExpression := expression ;
 * ----------------------------------------------------------------------- */

m2c_token_t while_statement (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("whileStatement");
  
  /* WHILE */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* boolExpression */
  if (match_set(p, FIRST(EXPRESSION), FOLLOW(WHILE_STATEMENT))) {
    lookahead = expression(p);
    
    /* DO */
    if (match_token(p, TOKEN_DO, FOLLOW(WHILE_STATEMENT))) {
      lookahead = m2c_consume_sym(p->lexer);
      
      /* check for empty statement sequence */
      if (lookahead == TOKEN_END) {

        /* empty statement sequence warning */
        m2c_emit_warning_w_pos
          (M2C_EMPTY_STMT_SEQ,
           m2c_lexer_lookahead_line(p->lexer),
           m2c_lexer_lookahead_column(p->lexer));
         
        /* END */
        lookahead = m2c_consume_sym(p->lexer);
      }

      /* statementSequence */
      else if
        (match_set(p, FIRST(STATEMENT_SEQUENCE), FOLLOW(WHILE_STATEMENT))) {
        lookahead = statement_sequence(p);
    
        /* END */
        if (match_token(p, TOKEN_END, FOLLOW(WHILE_STATEMENT))) {
          lookahead = m2c_consume_sym(p->lexer);
        } /* end if */
      } /* end if */
    } /* end if */
  } /* end if */
      
  return lookahead;
} /* end while_statement */


/* --------------------------------------------------------------------------
 * private procedure repeat_statement()
 * --------------------------------------------------------------------------
 * repeatStatement :=
 *   REPEAT statementSequence UNTIL boolExpression
 *   ;
 *
 * boolExpression := expression ;
 * ----------------------------------------------------------------------- */

m2c_token_t repeat_statement (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("repeatStatement");
  
  /* REPEAT */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* check for empty statement sequence */
  if (lookahead == TOKEN_UNTIL) {
  
    /* empty statement sequence warning */
    m2c_emit_warning_w_pos
      (M2C_EMPTY_STMT_SEQ,
       m2c_lexer_lookahead_line(p->lexer),
       m2c_lexer_lookahead_column(p->lexer));
  }
  
  /* statementSequence */
  else if (match_set(p,
           FIRST(STATEMENT_SEQUENCE), FOLLOW(STATEMENT_SEQUENCE))) {
    lookahead = statement_sequence(p);
  }
  else /* resync */ {
    lookahead = m2c_next_sym(p->lexer);
  } /* end if */
    
  /* UNTIL */
  if (match_token(p, TOKEN_UNTIL, FOLLOW(REPEAT_STATEMENT))) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* boolExpression */
    if (match_set(p, FIRST(EXPRESSION), FOLLOW(REPEAT_STATEMENT))) {
      lookahead = expression(p);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end repeat_statement */


/* --------------------------------------------------------------------------
 * private procedure for_statement()
 * --------------------------------------------------------------------------
 * forStatement :=
 *   FOR forLoopVariant ':=' startValue TO endValue
 *   ( BY stepValue )? DO statementSequence END
 *   ;
 *
 * forLoopVariant := Ident ;
 *
 * startValue, endValue := ordinalExpression ;
 *
 * ordinalExpression := expression
 *
 * stepValue := constExpression
 * ----------------------------------------------------------------------- */

m2c_token_t for_statement (m2c_parser_context_t p) {
  m2c_string_t ident;
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("forStatement");
  
  /* FOR */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* forLoopVariant */
  if (match_token(p, TOKEN_IDENTIFIER, RESYNC(FOR_LOOP_BODY))) {
    lookahead = m2c_consume_sym(p->lexer);
    ident = m2c_lexer_current_lexeme(p->lexer);
    
    /* ':=' */
    if (match_token(p, TOKEN_ASSIGN, RESYNC(FOR_LOOP_BODY))) {
      lookahead = m2c_consume_sym(p->lexer);
      
      /* startValue */
      if (match_set(p, FIRST(EXPRESSION), RESYNC(FOR_LOOP_BODY))) {
        lookahead = expression(p);
        
        /* TO */
        if (match_token(p, TOKEN_TO, RESYNC(FOR_LOOP_BODY))) {
          lookahead = m2c_consume_sym(p->lexer);
          
          /* endValue */
          if (match_set(p, FIRST(EXPRESSION), RESYNC(FOR_LOOP_BODY))) {
            lookahead = expression(p);
            
            /* ( BY stepValue )? */
            if (lookahead == TOKEN_BY) {
              lookahead = m2c_consume_sym(p->lexer);
              
              if (match_set(p, FIRST(EXPRESSION), RESYNC(FOR_LOOP_BODY))) {
                lookahead = const_expression(p);
              } /* end if */
            } /* end if */
          } /* end if */
        } /* end if */
      } /* end if */
    } /* end if */
  } /* end if */
  
  /* resync */
  lookahead = m2c_next_sym(p->lexer);
  
  /* DO -- The FOR loop body */
  if (match_token(p, TOKEN_DO, FOLLOW(FOR_STATEMENT))) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* check for empty statement sequence */
    if (lookahead == TOKEN_END) {

      /* empty statement sequence warning */
      m2c_emit_warning_w_pos
        (M2C_EMPTY_STMT_SEQ,
         m2c_lexer_lookahead_line(p->lexer),
         m2c_lexer_lookahead_column(p->lexer));
       
      /* END */
      lookahead = m2c_consume_sym(p->lexer);
    }

    /* statementSequence */
    else if
      (match_set(p, FIRST(STATEMENT_SEQUENCE), FOLLOW(FOR_STATEMENT))) {
      lookahead = statement_sequence(p);
  
      /* END */
      if (match_token(p, TOKEN_END, FOLLOW(FOR_STATEMENT))) {
        lookahead = m2c_consume_sym(p->lexer);
      } /* end if */
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end for_statement */


/* --------------------------------------------------------------------------
 * private procedure designator()
 * --------------------------------------------------------------------------
 * designator :=
 *   qualident selector*
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t selector (m2c_parser_context_t p);

m2c_token_t designator (m2c_parser_context_t p) {
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("designator");
  
  /* qualident */
  lookahead = qualident(p);
  
  /* selector* */
  while ((lookahead == TOKEN_DEREF) ||
         (lookahead == TOKEN_PERIOD) ||
         (lookahead == TOKEN_LEFT_BRACKET)) {
    lookahead = selector(p);
  } /* end if */
  
  return lookahead;
} /* end designator */


/* --------------------------------------------------------------------------
 * private procedure selector()
 * --------------------------------------------------------------------------
 * selector :=
 *   '^' | '.' Ident | '[' expressionList ']'
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t selector (m2c_parser_context_t p) {
  m2c_string_t ident;
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("selector");
  
  lookahead = m2c_next_sym(p->lexer);
      
  switch (lookahead) {
    
    /* '^' */
    case TOKEN_DEREF :
      lookahead = m2c_consume_sym(p->lexer);
      break;
      
    /* '.' Ident */
    case TOKEN_PERIOD :
      /* '.' */
      lookahead = m2c_consume_sym(p->lexer);
      
      /* Ident */
      if (match_token(p, TOKEN_IDENTIFIER, FOLLOW(SELECTOR))) {
        lookahead = m2c_consume_sym(p->lexer);
        ident = m2c_lexer_current_lexeme(p->lexer);
        /* TO DO: add ident to list structure */
      } /* end if */
      break;
      
    /* '[' */
    case TOKEN_LEFT_BRACKET :
      /* '[' */
      lookahead = m2c_consume_sym(p->lexer);
      
      /* expressionList ']' */
      if (match_set(p, FIRST(EXPRESSION), FOLLOW(SELECTOR))) {
        /* expressionList */
        lookahead = expression_list(p);
        
        /* ']' */
        if (match_token(p, TOKEN_RIGHT_BRACKET, FOLLOW(SELECTOR))) {
          lookahead = m2c_consume_sym(p->lexer);
        } /* end if */
      } /* end if */
      break;
      
    default : /* unreachable code */
      /* fatal error -- abort */
      exit(-1);
  } /* end switch */
      
  return lookahead;
} /* end selector */


/* --------------------------------------------------------------------------
 * private procedure expression()
 * --------------------------------------------------------------------------
 * expression :=
 *   simpleExpression ( operL1 simpleExpression )?
 *   ;
 *
 * operL1 := '=' | '#' | '<' | '<=' | '>' | '>=' | IN ;
 * ----------------------------------------------------------------------- */

#define IS_LEVEL1_OPERATOR(_t) \
  (((_t) == TOKEN_EQUAL) || ((_t) == TOKEN_NOTEQUAL) || \
   ((_t) == TOKEN_LESS) || ((_t) == TOKEN_LESS_EQUAL) || \
   ((_t) == TOKEN_GREATER) || ((_t) == TOKEN_GREATER_EQUAL) || \
   ((_t) == TOKEN_IN))

m2c_token_t simple_expression (m2c_parser_context_t p);

m2c_token_t expression (m2c_parser_context_t p) {
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("expression");
  
  /* simpleExpression */
  lookahead = simple_expression(p);
  
  /* ( operL1 simpleExpression )? */
  if (IS_LEVEL1_OPERATOR(lookahead)) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* simpleExpression */
    if (match_set(p, FIRST(EXPRESSION), FOLLOW(SIMPLE_EXPRESSION))) {
      lookahead = simple_expression(p);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end expression */


/* --------------------------------------------------------------------------
 * private procedure const_expression()
 * --------------------------------------------------------------------------
 * constExpression :=
 *   simpleExpression ( operL1 simpleExpression )?
 *   ;
 *
 * operL1 := '=' | '#' | '<' | '<=' | '>' | '>=' | IN ;
 * ----------------------------------------------------------------------- */

m2c_token_t const_expression (m2c_parser_context_t p) {
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("constExpression");
  
  /* simpleExpression */
  lookahead = simple_expression(p);
  
  /* ( operL1 simpleExpression )? */
  if (IS_LEVEL1_OPERATOR(lookahead)) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* simpleExpression */
    if (match_set(p, FIRST(EXPRESSION), FOLLOW(SIMPLE_EXPRESSION))) {
      lookahead = simple_expression(p);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end const_expression */


/* --------------------------------------------------------------------------
 * private procedure simple_expression()
 * --------------------------------------------------------------------------
 * simpleExpression :=
 *   ( '+' | '-' )? term ( operL2 term )*
 *   ;
 *
 * operL2 := '+' | '-' | OR ;
 * ----------------------------------------------------------------------- */

#define IS_LEVEL2_OPERATOR(_t) \
  (((_t) == TOKEN_PLUS) || ((_t) == TOKEN_MINUS) || ((_t) == TOKEN_OR))

m2c_token_t term (m2c_parser_context_t p);

m2c_token_t simple_expression (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("simpleExpression");
  
  lookahead = m2c_next_sym(p->lexer);
  
  /* ( '+' | '-' )? */
  if ((lookahead == TOKEN_PLUS) || (lookahead == TOKEN_MINUS)) {
    lookahead = m2c_consume_sym(p->lexer);
  } /* end if */
  
  /* term */
  if (match_set(p, FIRST(TERM), FOLLOW(TERM))) {
    lookahead = term(p);
  
    /* ( operL2 term )* */
    while (IS_LEVEL2_OPERATOR(lookahead)) {
      /* operL2 */
      lookahead = m2c_consume_sym(p->lexer);
    
      /* term */
      if (match_set(p, FIRST(TERM), FOLLOW(TERM))) {
        lookahead = term(p);
      } /* end if */
    } /* end while */
  } /* end if */
  
  return lookahead;
} /* end simple_expression */


/* --------------------------------------------------------------------------
 * private procedure term()
 * --------------------------------------------------------------------------
 * term :=
 *   simpleTerm ( operL3 simpleTerm )*
 *   ;
 *
 * operL3 := '*' | '/' | DIV | MOD | AND ;
 * ----------------------------------------------------------------------- */

#define IS_LEVEL3_OPERATOR(_t) \
  (((_t) == TOKEN_ASTERISK) || ((_t) == TOKEN_SOLIDUS) || \
   ((_t) == TOKEN_DIV) || ((_t) == TOKEN_MOD) || ((_t) == TOKEN_AND))

m2c_token_t simple_term (m2c_parser_context_t p);

m2c_token_t term (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("term");
  
  /* simpleTerm */
  lookahead = simple_term(p);
  
  /* ( operL3 simpleTerm )* */
  while (IS_LEVEL3_OPERATOR(lookahead)) {
    /* operL3 */
    lookahead = m2c_consume_sym(p->lexer);
    
    /* simpleTerm */
    if (match_set(p, FIRST(SIMPLE_TERM), FOLLOW(SIMPLE_TERM))) {
      lookahead = simple_term(p);
    } /* end if */
  } /* end while */
  
  return lookahead;
} /* end term */


/* --------------------------------------------------------------------------
 * private procedure simple_term()
 * --------------------------------------------------------------------------
 * simpleTerm :=
 *   NOT? factor
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t factor (m2c_parser_context_t p);

m2c_token_t simple_term (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("simpleTerm");
  
  lookahead = m2c_next_sym(p->lexer);
  
  /* NOT? */
  if (lookahead == TOKEN_NOT) {
    lookahead = m2c_consume_sym(p->lexer);
  } /* end if */
  
  /* factor */
  if (match_set(p, FIRST(FACTOR), FOLLOW(FACTOR))) {
    lookahead = factor(p);
  } /* end if */

  return lookahead;
} /* end simple_term */


/* --------------------------------------------------------------------------
 * private procedure factor()
 * --------------------------------------------------------------------------
 * factor :=
 *   NumberLiteral | StringLiteral | setValue |
 *   designatorOrFuncCall | '(' expression ')'
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t set_value (m2c_parser_context_t p);

m2c_token_t designator_or_func_call (m2c_parser_context_t p);

m2c_token_t factor (m2c_parser_context_t p) {
  m2c_string_t lexeme;
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("factor");
  
  lookahead = m2c_next_sym(p->lexer);
  
  switch (lookahead) {
  
    /* NumberLiteral */
    case TOKEN_INTEGER :
    case TOKEN_REAL :
    case TOKEN_CHAR :
      lookahead = m2c_consume_sym(p->lexer);
      lexeme = m2c_lexer_current_lexeme(p->lexer);
      break;
      
    /* | StringLiteral */
    case TOKEN_STRING :
      lookahead = m2c_consume_sym(p->lexer);
      lexeme = m2c_lexer_current_lexeme(p->lexer);
      break;
      
    /* | setValue */
    case TOKEN_LEFT_BRACE :
      lookahead = set_value(p);
      break;
      
    /* | designatorOrFuncCall */
    case TOKEN_IDENTIFIER :
      lookahead = designator_or_func_call(p);
      break;
      
    /* | '(' expression ')' */
    case TOKEN_LEFT_PAREN :
      lookahead = m2c_consume_sym(p->lexer);
      
      /* expression */
      if (match_set(p, FIRST(EXPRESSION), FOLLOW(FACTOR))) {
        lookahead = expression(p);
        
        /* ')' */
        if (match_token(p, TOKEN_RIGHT_PAREN, FOLLOW(FACTOR))) {
          lookahead = m2c_consume_sym(p->lexer);
        } /* end if */
      } /* end if */
      break;
      
    default : /* unreachable code */
      /* fatal error -- abort */
      exit(-1);
  } /* end switch */
  
  return lookahead;
} /* end factor */


/* --------------------------------------------------------------------------
 * private procedure designator_or_func_call()
 * --------------------------------------------------------------------------
 * designatorOrFuncCall :=
 *   designator ( setValue | '(' expressionList? ')' )?
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t designator_or_func_call (m2c_parser_context_t p) {
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("designatorOrFuncCall");
  
  /* designator */
  lookahead = designator(p);
  
  /* setValue */
  if (lookahead == TOKEN_LEFT_BRACE) {
    lookahead = set_value(p);
    
  }
  /* '(' expressionList? ')' */
  else if (lookahead == TOKEN_LEFT_PAREN) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* expressionList? */
    if (m2c_tokenset_element(FIRST(EXPRESSION), lookahead)) {
      lookahead = expression_list(p);
    } /* end if */
    
    if (match_token(p, TOKEN_RIGHT_PAREN, FOLLOW(DESIGNATOR_OR_FUNC_CALL))) {
      lookahead = m2c_consume_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end designator_or_func_call */


/* --------------------------------------------------------------------------
 * private procedure set_value()
 * --------------------------------------------------------------------------
 * setValue :=
 *   '{' element ( ',' element )* '}'
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t element (m2c_parser_context_t p);

m2c_token_t set_value (m2c_parser_context_t p) {
  m2c_token_t lookahead;
  
  PARSER_DEBUG_INFO("setValue");
  
  /* '{' */
  lookahead = m2c_consume_sym(p->lexer);
  
  /* element */
  if (match_set(p, FIRST(ELEMENT), FOLLOW(SET_VALUE))) {
    lookahead = element(p);
    
    /* ( ',' element )* */
    while (lookahead == TOKEN_COMMA) {
      /* ',' */
      lookahead = m2c_consume_sym(p->lexer);
    
      /* element */
      if (match_set(p, FIRST(ELEMENT), FOLLOW(SET_VALUE))) {
        lookahead = element(p);
      }
      else /* resync */ {
        lookahead = m2c_next_sym(p->lexer);
      } /* end if */
    } /* end while */
    
    /* '}' */
    if (match_token(p, TOKEN_RIGHT_BRACE, FOLLOW(SET_VALUE))) {
      lookahead = m2c_consume_sym(p->lexer);
    } /* end if */
  } /* end if */
    
  return lookahead;
} /* end set_value */


/* --------------------------------------------------------------------------
 * private procedure element()
 * --------------------------------------------------------------------------
 * element :=
 *   expression ( '..' expression )?
 *   ;
 * ----------------------------------------------------------------------- */

m2c_token_t element (m2c_parser_context_t p) {
  m2c_token_t lookahead;
    
  PARSER_DEBUG_INFO("element");
  
  /* expression */
  lookahead = expression(p);
  
  /* ( '..' expression )? */
  if (lookahead == TOKEN_RANGE) {
    lookahead = m2c_consume_sym(p->lexer);
    
    /* expression */
    if (match_set(p, FIRST(EXPRESSION), FOLLOW(ELEMENT))) {
      lookahead = expression(p);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end element */

/* END OF FILE */