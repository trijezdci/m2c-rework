/* M2C Modula-2 Compiler & Translator
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * M2C is a compiler and translator for the classic Modula-2 programming
 * language as described in the 3rd and 4th editions of Niklaus Wirth's
 * book "Programming in Modula-2" (PIM) published by Springer Verlag.
 *
 * In compiler mode, M2C compiles Modula-2 source via C to object files or
 * executables using the host system's resident C compiler and linker.
 * In translator mode, it translates Modula-2 source to C source.
 *
 * Further information at http://savannah.nongnu.org/projects/m2c/
 *
 * @file
 *
 * m2c.c
 *
 * M2C main program.
 *
 * @license
 *
 * M2C is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation;  either version 2 of the License (GPL2),
 * or (at your option) any later version.
 *
 * M2C is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with m2c.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "m2-lexer.h"
#include "m2-error.h"
#include "m2-fileutils.h"
#include "m2-parser.h"
#include "m2-unique-string.h"
#include "m2-compiler-options.h"

#include <stdio.h>
#include <stdlib.h>


#define M2C_IDENTIFICATION "m2c Modula-2 Compiler & Translator"

#define M2C_VERSION_INFO "version 1.00"

#define M2C_COPYRIGHT \
  "copyright (c) 2015 B.Kowarsch"

#define M2C_LICENSE \
  "licensed under the GNU General Public License v.2 and v.3"


static void print_identification(void) {
  printf(M2C_IDENTIFICATION ", " M2C_VERSION_INFO "\n");
} /* end print_identification */


static void print_version(void) {
  printf(M2C_VERSION_INFO ", build (%05u)\n", M2C_BUILD_NUMBER);
} /* end print_version */


static void print_copyright(void) {
  printf(M2C_COPYRIGHT "\n");
} /* end print_copyright */


static void print_license(void) {
  printf(M2C_LICENSE "\n");
} /* end print_license */


static void print_usage(void) {
  printf("usage:\n");
  printf(" m2c sourcefile [options]\n");
} /* end print_usage */


static void exit_with_usage(void) {
  print_usage();
  printf(" or m2c -h for help\n");
  exit(EXIT_FAILURE);
} /* end exit_with_usage */


static void exit_with_help(void) {
  print_identification();
  print_copyright();
  print_license();
  print_usage();
  m2c_print_option_help();
  exit(EXIT_SUCCESS);
} /* end print_help */


static void exit_with_version(void) {
  print_version();
  exit(EXIT_SUCCESS);
} /* end exit_with_version */


#define IS_DEF(_fn) \
  ((_fn[0] == '.') && (_fn[4] == 0) && \
   (((_fn[1] == 'd') && (_fn[2] == 'e') && (_fn[3] == 'f')) || \
    ((_fn[1] == 'D') && (_fn[2] == 'E') && (_fn[3] == 'F'))))


#define IS_MOD(_fn) \
  ((_fn[0] == '.') && (_fn[4] == 0) && \
   (((_fn[1] == 'm') && (_fn[2] == 'o') && (_fn[3] == 'd')) || \
    ((_fn[1] == 'M') && (_fn[2] == 'O') && (_fn[3] == 'D'))))


int main (int argc, char *argv[]) {
  m2c_string_t filename;
  const char *fn, *file_suffix;
  uint_t error_count = 0;
  m2c_option_status_t cli_status;
  m2c_parser_status_t parser_status;
  
  if (argc < 2) {
    exit_with_usage();
  } /* end if */
  
  /* get command line arguments and filename */
  fn = m2c_get_cli_args(argc, argv, &cli_status);
  
  /* check for failure, help or version request */
  if (cli_status == M2C_OPTION_STATUS_FAILURE) {
    exit_with_usage();
  }
  else if (cli_status == M2C_OPTION_STATUS_HELP_REQUESTED) {
    exit_with_help();
  }
  else if (cli_status == M2C_OPTION_STATUS_VERSION_REQUESTED) {
    exit_with_version();
  } /* end if */
  
  /* check filename and file availability */
  if ((fn == NULL) || (fn[0] == ASCII_NUL)) {
    m2c_emit_error(M2C_ERROR_MISSING_FILENAME);
    exit(EXIT_FAILURE);
  }
  else if (!is_valid_pathname(fn)) {
    m2c_emit_error_w_str(M2C_ERROR_INVALID_FILENAME, fn);
    exit(EXIT_FAILURE);
  }
  else if (!file_exists(fn)) {
    m2c_emit_error_w_str(M2C_ERROR_INPUT_FILE_NOT_FOUND, fn);
    exit(EXIT_FAILURE);
  } /* end if */
  
  /* print banner */
  print_identification();
  
  if (m2c_option_parser_debug()) {
    m2c_print_options();
  } /* end if */
  
  /* initialise string repo */
  m2c_init_string_repository(0, NULL);
  
  /* register filename */
  filename = m2c_get_string((char *) fn, NULL);
    
  /* get suffix */
  file_suffix = filetype(fn);
  
  if (file_suffix == NULL) {
    printf("invalid filename, suffix must be .def, .DEF, .mod or .MOD\n");
    exit(EXIT_FAILURE);
  } /* end if */
  
  printf("processing %s\n", fn);
  
  /* run parser on input */
  if (IS_DEF(file_suffix)) {
    m2c_syntax_check_def(filename, &parser_status, &error_count);
    printf("parse error count: %u\n", error_count);
  }
  else if (IS_MOD(file_suffix)) {
    m2c_syntax_check_mod(filename, &parser_status, &error_count);
    printf("parse error count: %u\n", error_count);
  }
  else {
    printf("invalid filename, suffix must be .def, .DEF, .mod or .MOD\n");
    exit(EXIT_FAILURE);
  } /* end if */
  
  return EXIT_SUCCESS;
} /* end main */

/* END OF FILE */