/* M2C Modula-2 Compiler & Translator
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * M2C is a compiler and translator for the classic Modula-2 programming
 * language as described in the 3rd and 4th editions of Niklaus Wirth's
 * book "Programming in Modula-2" (PIM) published by Springer Verlag.
 *
 * In compiler mode, M2C compiles Modula-2 source via C to object files or
 * executables using the host system's resident C compiler and linker.
 * In translator mode, it translates Modula-2 source to C source.
 *
 * Further information at http://savannah.nongnu.org/projects/m2c/
 *
 * @file
 *
 * m2-common.h
 *
 * Common definitions.
 *
 * @license
 *
 * M2C is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation;  either version 2 of the License (GPL2),
 * or (at your option) any later version.
 *
 * M2C is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with m2c.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef M2C_COMMON_H
#define M2C_COMMON_H

#include <stdint.h>


/* --------------------------------------------------------------------------
 * Common constants
 * ----------------------------------------------------------------------- */

#define ASCII_NUL 0

#define ASCII_EOT 4

#define ASCII_TAB 9

#define ASCII_LF 10

#define ASCII_CR 13

#define ASCII_SPACE 32

#define EMPTY_STRING "\0"


/* --------------------------------------------------------------------------
 * Common macros
 * ----------------------------------------------------------------------- */

#define NOT(_expr) \
  (!(_expr))

#define CAST(_type,_var) \
  ((_type) _var)

#define SET_STATUS(_status_ptr,_value) \
  { if (_status_ptr != NULL) {*_status_ptr = _value; }; }


/* --------------------------------------------------------------------------
 * Common types
 * ----------------------------------------------------------------------- */

typedef unsigned int uint_t;

typedef uint8_t m2c_octet_t;

typedef unsigned char m2c_char_t;

typedef unsigned int m2c_cardinal_t;

typedef int m2c_integer_t;

typedef long int m2c_longint_t;

typedef float m2c_real_t;

typedef double m2c_longreal_t;

typedef void *m2c_address_t;


/* ==========================================================================
 * Verify build parameters
 * ======================================================================= */

#include "m2-build-params.h"


/* --------------------------------------------------------------------------
 * Verify M2C_TARGET_OS
 * ----------------------------------------------------------------------- */

#if !defined(M2C_TARGET_OS)
#error "no value defined for M2C_TARGET_OS"
#elif (M2C_TARGET_OS < OS_TYPE_MIN_VALUE) || \
      (M2C_TARGET_OS > OS_TYPE_MAX_VALUE)
#error "incorrect value for M2C_TARGET_OS"
#endif


/* --------------------------------------------------------------------------
 * Verify M2C_TARGET_FILENAMING
 * ----------------------------------------------------------------------- */

#if !defined(M2C_TARGET_FILENAMING)
#error "no value defined for M2C_TARGET_FILENAMING"
#elif (M2C_TARGET_FILENAMING < FILENAMING_MIN_VALUE) || \
      (M2C_TARGET_FILENAMING > FILENAMING_MAX_VALUE)
#error "incorrect value for M2C_TARGET_FILENAMING"
#endif


/* --------------------------------------------------------------------------
 * Verify M2C_PATHNAME_BUFFER_SIZE
 * ----------------------------------------------------------------------- */

#if !defined(M2C_PATHNAME_BUFFER_SIZE)
#error "no value defined for M2C_PATHNAME_BUFFER_SIZE"
#endif


/* --------------------------------------------------------------------------
 * Verify M2C_COROUTINES_IMPLEMENTED
 * ----------------------------------------------------------------------- */

#if !defined(M2C_COROUTINES_IMPLEMENTED)
#error "no value defined for M2C_COROUTINES_IMPLEMENTED"
#endif


/* --------------------------------------------------------------------------
 * Verify M2C_LOCAL_MODULES_IMPLEMENTED
 * ----------------------------------------------------------------------- */

#if !defined(M2C_LOCAL_MODULES_IMPLEMENTED)
#error "no value defined for M2C_LOCAL_MODULES_IMPLEMENTED"
#endif


#endif /* M2C_COMMON_H */

/* END OF FILE */