/* M2C Modula-2 Compiler & Translator
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * M2C is a compiler and translator for the classic Modula-2 programming
 * language as described in the 3rd and 4th editions of Niklaus Wirth's
 * book "Programming in Modula-2" (PIM) published by Springer Verlag.
 *
 * In compiler mode, M2C compiles Modula-2 source via C to object files or
 * executables using the host system's resident C compiler and linker.
 * In translator mode, it translates Modula-2 source to C source.
 *
 * Further information at http://savannah.nongnu.org/projects/m2c/
 *
 * @file
 *
 * m2-fileutils.c
 *
 * File system utility functions.
 *
 * @license
 *
 * M2C is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation;  either version 2 of the License (GPL2),
 * or (at your option) any later version.
 *
 * M2C is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with m2c.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "m2-fileutils.h"

#include "m2-common.h"

#include <stdlib.h>
#include <sys/stat.h>


/* --------------------------------------------------------------------------
 * Get target OS specific headers
 * ----------------------------------------------------------------------- */

#if M2C_TARGET_OS == OS_TYPE_POSIX
#include <unistd.h>
#elif M2C_TARGET_OS == OS_TYPE_DOS
#error "DOS not yet supported"
#elif M2C_TARGET_OS == OS_TYPE_VMS
#error "VMS not yet supported"
#elif M2C_TARGET_OS == OS_TYPE_WIN
#include <direct.h>
#else
#error "M2C_TARGET_OS has invalid value"
#endif


/* --------------------------------------------------------------------------
 * function is_valid_filename(filename)
 * --------------------------------------------------------------------------
 * Returns true if filename is a valid filename, otherwise false.
 * --------------------------------------------------------------------------
 */

#define IS_FILENAME_CHAR(_ch) \
  (((_ch) == '.') || ((_ch) == '_') || \
  (((_ch) >= 'a') && ((_ch) <= 'z')) || \
  (((_ch) >= '0') && ((_ch) <= '9')) || \
  (((_ch) >= 'A') && ((_ch) <= 'Z')))
  
bool is_valid_filename (const char *filename) {
  uint_t index = 0;
  
  if (filename[0] == ASCII_NUL) {
   return false;
  } /* end if */
    
  while (IS_FILENAME_CHAR(filename[index])) {
    index++;
    if ((filename[index] == '.') && (filename[index-1] == '.')) {
      break;
    } /* end if */
  } /* end while */
  
  return (filename[index] == ASCII_NUL);
} /* end is_valid_filename */


/* --------------------------------------------------------------------------
 * function is_valid_pathname(pathname)
 * --------------------------------------------------------------------------
 * Returns true if pathname is a valid pathname, otherwise false.
 * --------------------------------------------------------------------------
 */

bool is_valid_pathname (const char *pathname) {
  uint_t index = 0;
  
  if (pathname[0] == ASCII_NUL) {
   return false;
  } /* end if */
  
  /* advance to first slash */
  if (pathname[0] == '~') {
    index = 1;
    if (pathname[1] != '/') {
      return false;
    } /* end if */
  }
  else if (pathname[0] == '.') {
    if (pathname[1] == '/') {
      index = 1;
    }
    else if (pathname[1] == '.') {
      index = 2;
      if (pathname[2] != '/') {
        return false;
      } /* end if */
    } /* end if */
  } /* end if */
  
  if (pathname[index] == '/') {
    /* advance over subdirectories */
    while (pathname[index] == '/') {
      index++;
      while (IS_FILENAME_CHAR(pathname[index])) {
        index++;
        if ((pathname[index] == '.') && (pathname[index-1] == '.')) {
          return false;
        } /* end if */
      } /* end while */
    } /* end while */
  }
  else {
    /* advance over filename */
    while (IS_FILENAME_CHAR(pathname[index])) {
      index++;
      if ((pathname[index] == '.') && (pathname[index-1] == '.')) {
        return false;
      } /* end if */
    } /* end while */
  } /* end if */
  
  return (pathname[index] == ASCII_NUL);
} /* end is_valid_pathname */


/* --------------------------------------------------------------------------
 * function file_exists(filename)
 * --------------------------------------------------------------------------
 * Returns true if the file indicated by filename exists, otherwise false.
 * --------------------------------------------------------------------------
 */

bool file_exists (const char *filename) {
#if M2C_TARGET_OS == OS_TYPE_POSIX
  return (access(filename, F_OK) == 0);
#elif M2C_TARGET_OS == OS_TYPE_WIN
#error "not implemented"
#else
#error "not implemented"
#endif
} /* end file_exists */


/* --------------------------------------------------------------------------
 * function filesize(filename)
 * --------------------------------------------------------------------------
 * Returns the file size of a file.  Returns INVALID_FILE_SIZE on failure.
 * --------------------------------------------------------------------------
 */

int filesize (const char *filename) {
  struct stat st;
  
  if (filename != NULL) {
    stat(filename, &st);
    return st.st_size;
  }
  else {
    return INVALID_FILE_SIZE;
  } /* end if */
} /* end filesize */


/* --------------------------------------------------------------------------
 * function filetype(filename)
 * --------------------------------------------------------------------------
 * Returns a pointer to the position of the last period in filename.
 * Returns NULL if filename is NULL or if it does not contain any period.
 * ----------------------------------------------------------------------- */

const char *filetype (const char *filename) {
  char *running_ptr, *period_ptr;
  
  if (filename == NULL) {
   return NULL;
  } /* end if */
  
  period_ptr = NULL;
  running_ptr = (char *) filename;
  while (*running_ptr != ASCII_NUL) {
    if (*running_ptr == '/') {
      period_ptr = NULL;
    }
    else if (*running_ptr == '.') {
      period_ptr = running_ptr;
    } /* end if */
    running_ptr++;
  } /* end while */
  
  return (const char *) period_ptr;
} /* end filetype */


/* --------------------------------------------------------------------------
 * function macro get_cwd(cwd, size)
 * --------------------------------------------------------------------------
 * Obtains current directory path in cwd and returns a pointer to it.
 * --------------------------------------------------------------------------
 */

char *get_cwd(char *cwd, size_t size) {
#if M2C_TARGET_OS == OS_TYPE_POSIX
  return getcwd(cwd, size);
#elif M2C_TARGET_OS == OS_TYPE_WIN
  return _getcwd(cwd, size);
#else
#endif
} /* end get_cwd */

/* END OF FILE */